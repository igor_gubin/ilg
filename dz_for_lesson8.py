# Статические методы и методы класса
class Dog:
    def __init__(self, name, age=None, owner_name=None):
        self._name = name
        self._owner_name = owner_name
        self._age = None
        if not age is None:
            self._age = float(age)

    @classmethod
    def create_ownerless(cls, name):
        return cls(name)

    @staticmethod
    def is_mature(age):
        if age is None:
            return None
        return float(age) > 2

    def get_name(self):
        return self._name

    def get_age(self):
        return self._age

    def get_owner_name(self):
        return self._owner_name

# Статические атрибуты класса
class Transport:
    isinstances_count = 0
    def __init__(self, name, capacity):
        self._name = name
        self._capacity = capacity
        self.pas_on_board = 0
        Transport.isinstances_count += 1

    def on_out(self, num):
        free = self._capacity - self.pas_on_board
        if num > 0:
            oncnt = 0
            if num > free:
                oncnt = free
            else:
                oncnt = num
            self.pas_on_board += oncnt
            print(f"Вошло {oncnt} человек")
        elif num < 0:
            num *= -1
            outcnt = 0
            if self.pas_on_board > num:
                outcnt = num
            elif self.pas_on_board > 0:
                outcnt = self.pas_on_board

            self.pas_on_board -= outcnt
            print(f"Вышло {outcnt} человек")

    def __del__(self):
        Transport.isinstances_count -= 1

    @classmethod
    def get_count(cls):
        return Transport.isinstances_count



# Свойства "в классическом виде"
class ClassicPropertyClass:
    def __init__(self, num):
        self._num = int(num)

    def get_num(self):
        return self._num

    def set_num(self, num):
        self._num = num

    def del_num(self):
        self._num = None

    num = property(get_num, set_num, del_num, "целое число")


# Свойства для метода
class Cat:
    def __init__(self, name, age, owner_name, favourite_toys):
        """
        favourite_toys (dict в виде наименование: количество в наличии), name, age, owner_name
        """
        self._favourite_toys = dict(favourite_toys)
        self._name = name
        self._age = age
        self._owner_name = owner_name

    @property
    def info(self):
        """
        возвращает строку с полной информацией о котике
        """
        return f"Кличка: \"{self._name}\"; Возраст: {self._age}({self.age_level()}); Хозяин: \"{self._owner_name}\";" + \
               f" Популярная игрушка: \"{self.popular_toy}\""

    @property
    def toys(self):
        """
        возвращает строку со списком всех игрушек
        """
        return ", ".join(self._favourite_toys.keys())

    @property
    def popular_toy(self):
        """
        Возвращает игрушку, которой больше всего в наличии
        """
        res = ""
        max = 0
        for k, v in self._favourite_toys.items():
            if max < v:
                res = k
                max = v
        return  res

    def age_level(self):
        """
        печатает “котенок”, “молодой”, “взрослый”, “старый” в зависимости от age
        """
        if 0 <= self._age < 1:
            return "котенок"
        elif 1 <= self._age < 3:
            return "молодой"
        elif 3 <= self._age < 10:
            return "взрослый"
        elif 10 <= self._age:
            return "старый"
        else:
            return "возраст указан не верно"

# Классы, наследование, свойства (декоратор), исключения
class Person:
    def __init__(self, name, age):
        self._name = name
        self._age = age

    name = property()
    age = property()

    @name.getter
    def name(self):
        """
        ФИО
        """
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @name.deleter
    def name(self):
        self._name = None

    @age.getter
    def age(self):
        """
        Возраст
        """
        return self._age

    @age.setter
    def age(self, age):
        self._age = float(age)

    @age.deleter
    def age(self):
        self._age = None

class Programmer(Person):
    @classmethod
    def statuses(cls):
        return  {"junior":1,"middle":2,"senior":3}

    def __init__(self, name, age, prog_languages, status, company):
        Person.__init__(self, name, age)
        self._prog_languages = list(prog_languages)
        self._status = self.statuses()[status]
        self._company = str(company)
        
    prog_languages = property()
    status = property()
    company = property()

    @prog_languages.getter
    def prog_languages(self):
        """
        Языки программирования
        """
        return self._prog_languages


    @prog_languages.setter
    def prog_languages(self, prog_languages):
        self._prog_languages = list(prog_languages)

    @prog_languages.deleter
    def prog_languages(self):
        self._prog_languages = None

    def prog_languages_extend(self, prog_languages):
        self._prog_languages.extend(prog_languages)

    @status.getter
    def status(self):
        """
        Статус
        """
        res = None
        for k, v in self.statuses().items():
            if self._status == v:
                res = k
        return res

    @status.setter
    def status(self, status):
        self._status = self.statuses()[status]

    @status.deleter
    def status(self):
        self._status = None
        
    @company.getter
    def company(self):
        """
        Компания в которой работает
        """
        return self._company

    @company.setter
    def company(self, company):
        self._company = str(company)

    @company.deleter
    def company(self):
        self._company = None

class Manager(Person):
    def __init__(self, name, age, subordinates, respect_level):
        Person.__init__(self, name, age)
        self._subordinates = list(subordinates)
        self._respect_level = Manager.get_respect_level(respect_level)

    subordinates = property()
    respect_level = property()

    @subordinates.getter
    def subordinates(self):
        """

        Подчиненные
        """
        return self._subordinates

    @subordinates.setter
    def subordinates(self, subordinates):
        self._subordinates = list(subordinates)

    @subordinates.deleter
    def subordinates(self):
        self._subordinates = None

    def subordinates_extend(self, subordinates):
        self._subordinates.extend(subordinates)

    @respect_level.getter
    def respect_level(self):
        """
        Уровень авторитета
        """
        return self._respect_level

    @respect_level.setter
    def respect_level(self, respect_level):
        self._respect_level = Manager.get_respect_level(respect_level)

    @respect_level.deleter
    def respect_level(self):
        self._respect_level = None

    @staticmethod
    def get_respect_level(respect_level):
        tmp_respect_level = int(respect_level)
        if tmp_respect_level < 0 or tmp_respect_level > 100:
            raise ValueError(f"respect_level вне диаппазона [0, 100]")
        return tmp_respect_level

class TeamLeader(Programmer, Manager):
    def __init__(self, name, age, prog_languages, status, company, subordinates, respect_level, star_level):
        Person.__init__(self, name, age)
        Programmer.__init__(self, name, age, prog_languages, status, company)
        Manager.__init__(self, name, age, subordinates, respect_level)
        self._star_level = TeamLeader.get_star_level(star_level)

    star_level = property()

    @star_level.getter
    def star_level(self):
        """
        Уровень звездности [0,5]
        """
        return self._star_level

    @star_level.setter
    def star_level(self, star_level):
        self._star_level = TeamLeader.get_star_level(star_level)

    @star_level.deleter
    def star_level(self):
        self._star_level = None

    @staticmethod
    def get_star_level(star_level):
        tmp_star_level = int(star_level)
        if tmp_star_level < 0 or tmp_star_level > 5:
            raise ValueError(f"star_level вне диаппазона [0, 5]")
        return tmp_star_level

# Исключения
class NotValidAge(Exception):
    pass

def is_adult(age):
    """
    Возвращает True, если age >= 18, иначе False.
    """
    age = float(age)
    if age < 0 or age > 150:
        raise NotValidAge("Значение age должно быть между 0 и 150 годами")

    return age >= 18


def main():
    while True:
        tn = input("Введите номер задачи от 1 до 6 (для выхода любую другую клавишу): ")
        if tn == "1":
            print("Статические методы и методы класса")
            dogs = [Dog("Дымок", 3, "Иванов И.И."), Dog("Стёпка", 1, "Петров П.П."), Dog.create_ownerless("Ями")]
            for d in dogs:
                age_level = Dog.is_mature(d.get_age())
                age_level_str = "взрослый" if age_level == True else "None" if age_level is None else "молодой"
                print(f"Кличка: \"{d.get_name()}\"; Возраст: {d.get_age()}({age_level_str});" + \
                      f" Хозяин: {d.get_owner_name()}")

        elif tn == "2":
            print("Статические атрибуты класса")
            transports = [Transport("T9", 12), Transport("T131", 40), Transport("T10", 40)]
            print(f"Всего: {Transport.get_count()}")
            del transports[0]
            print(f"Всего: {Transport.get_count()}")
            del transports[0]
            print(f"Всего: {Transport.get_count()}")
            del transports[0]
            print(f"Всего: {Transport.get_count()}")
            t = Transport("T131", 40)
            transports.extend([t])
            print(f"Всего: {Transport.get_count()}")
            t.on_out(45)
            t.on_out(-15)
            t.on_out(-15)
            t.on_out(-15)


        elif tn == "3":
            print("Свойства \"в классическом виде\"")
            c = ClassicPropertyClass(23)
            print(f"{type(c).__name__.strip()} : Num: {c.num}:")
        elif tn == "4":
            print("Свойства для метода")
            cats = [Cat("мурка", 0.5, "Мальцев И.И.", {"шерстяная нить": 3, "мяч": 1, "плюшевая мышь": 2}), \
                    Cat("васька", 2, "Харламов И.И.", {"шерстяная нить": 1, "мяч": 3, "плюшевая мышь": 1}), \
                    Cat("муся", 6, "Ларионов И.И.", {"шерстяная нить": 1, "мяч": 2, "плюшевая мышь": 5}), \
                    Cat("пушок", 11, "Михайлов И.И.", {"шерстяная нить": 4, "мяч": 1, "плюшевая мышь": 4}), \
                    ]
            for cat in cats:
                print(cat.info)
                print("Игрушки: ", cat.toys)

        elif tn == "5":
            print("Классы, наследование, свойства (декоратор), исключения")

            p1 = Programmer("Иванов И.И.", 21, ["py", "c#"], "junior", "BitWarp LLC")
            p1.prog_languages_extend(["c++"])
            p2 = Programmer("Петров П.П.", 22, ["py", "javascript"], "middle", "BitWarp LLC")
            p3 = Programmer("Сидоров С.С.", 33, ["py", "c++"], "senior", "BitWarp LLC")
            tl = TeamLeader("Михайлов М.М.", 28, ["py", "c#", "c++", "javascript"], \
                            "senior", "BitWarp LLC", [p1,p2], 70, 4)
            tl.subordinates_extend([p3])
            mn = Manager("Борис Б.Б.", 44, [p1,p2,p3], 99)
            mn.subordinates_extend([tl])

            print(f"{type(p1).__name__.strip()}: {Programmer.name.__doc__.strip()}:\"{p1.name}\"; " + \
                f"{Programmer.age.__doc__.strip()}: {p1.age}; {Programmer.status.__doc__.strip()}:\"{p1.status}\"; " \
                f"{Programmer.prog_languages.__doc__.strip()}:\"{p1.prog_languages}\";")
            print(f"{type(p2).__name__.strip()}: {Programmer.name.__doc__.strip()}:\"{p2.name}\"; " + \
                f"{Programmer.age.__doc__.strip()}: {p2.age}; {Programmer.status.__doc__.strip()}:\"{p2.status}\"; " \
                f"{Programmer.prog_languages.__doc__.strip()}:\"{p2.prog_languages}\";")
            print(f"{type(p3).__name__.strip()}: {Programmer.name.__doc__.strip()}:\"{p3.name}\"; " + \
                f"{Programmer.age.__doc__.strip()}: {p3.age}; {Programmer.status.__doc__.strip()}:\"{p3.status}\"; " \
                f"{Programmer.prog_languages.__doc__.strip()}:\"{p3.prog_languages}\";")
            print(f"{type(tl).__name__.strip()}: {Programmer.name.__doc__.strip()}:\"{tl.name}\"; " + \
                f"{Programmer.age.__doc__.strip()}: {tl.age}; {Programmer.status.__doc__.strip()}:\"{tl.status}\"; " \
                f"{Programmer.prog_languages.__doc__.strip()}:\"{tl.prog_languages}\";")
            print("\t", f"{Manager.respect_level.__doc__.strip()}: {tl.respect_level}%")
            print("\t", f"{Manager.subordinates.__doc__.strip()}:{[x.name for x in tl.subordinates]}")
            print(f"{type(mn).__name__.strip()}: {Programmer.name.__doc__.strip()}:\"{mn.name}\"; " + \
                  f"{Programmer.age.__doc__.strip()}: {mn.age};")
            print("\t", f"{Manager.respect_level.__doc__.strip()}: {mn.respect_level}%")
            print("\t", f"{Manager.subordinates.__doc__.strip()}:{[x.name for x in mn.subordinates]}")

        elif tn == "6":
            print("Исключения")
            help(is_adult)
            ages = [0.5, 1, 17, 18, 19, "one", "eighteen", -1, 151, 200]
            for av in ages:
                try:
                    print(av, ":", is_adult(av))
                except Exception as ex:
                    print(av, ":", ex)
        else:
            break


if __name__ == "__main__":
    main()