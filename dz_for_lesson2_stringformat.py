def as_float(sv):
    res = 0
    try:
        res = float(sv)
    except:
        res = None
    return res


def print_hello_name_age(first_name, last_name, age):
    """
    Принимает first_name, last_name, age.
    Возвращает строку в виде “Здравствуйте, Иванов Петр. Как хорошо, что вам всего 66!”.
    """
    return "Здравствуйте, {1} {0}. Как хорошо, что вам всего {2}!".format(first_name, last_name, age)


def print_coordinates(sx, sy):
    """
    Принимает координаты x, y. Возвращает строку в виде “Цель: 56.879, 22.554”.
    """
    x = as_float(sx)
    y = as_float(sy)
    return "Цель: {}, {}".format(x, y)


def print_name_error(name, err_no):
    """
    Принимает name, err_no. Возвращает строку в виде “Hi, name. There is a 0xerr_no!”.
    (Например, при входных параметрах Ellen, 404 будет получена строка “Hi, Ellen. There is a 0x404!”).
    """
    return "Hi, {}. There is a 0x{}!".format(name, err_no)


def main():
    while True:
        tn = input("Введите номер задачи Первого задания от 1 до 3 (для выхода любую другую клавишу): ")
        if tn == "1":
            help(print_hello_name_age)
            a = input("Введите имя: ")
            b = input("Введите фамилию: ")
            c = input("Введите возраст: ")
            print(print_hello_name_age(a, b, c))
        elif tn == "2":
            help(print_coordinates)
            a = input("Введите координату X: ")
            b = input("Введите координату Y: ")
            print(print_coordinates(a, b))
        elif tn == "3":
            help(print_name_error)
            a = input("Введите имя: ")
            b = input("Введите номер ошибки: ")
            print(print_name_error(a, b))


if __name__ == "__main__":
    main()
