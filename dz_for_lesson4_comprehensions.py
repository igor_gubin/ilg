def gen_list_double(n):
    """
    Принимает число n. Возвращает список длиной n, состоящий из удвоенных значений от 0 до n.
    Пример: n=3, результат [0, 2, 4].
    """
    if n is None:
        return None
    return [x * 2 for x in range(0, n)]


def gen_list_double_start_stop(start, stop):
    """
    Принимает числа start, stop. Возвращает список состоящий из удвоенных значений от start до stop (не включая).
    Пример: start=3, stop=6, результат [6, 8, 10].
    """
    if start is None or stop is None:
        return None
    return [x * 2 for x in range(start, stop)]


def gen_list_pow_start_stop(start, stop):
    """
    Принимает числа start, stop. Возвращает список состоящий из квадратов значений от start до stop (не включая).
    """
    if start is None or stop is None:
        return None
    return [x ** 2 for x in range(start, stop)]


def gen_list_pow_even(n):
    """
    Принимает число n. Возвращает список длиной n, состоящий из квадратов четных чисел в диапазоне от 0 до n.
    """
    if n is None:
        return None
    return [x ** 2 for x in range(0, n) if x % 2 == 0]


def gen_list_pow_odd(n):
    """
    Принимает число n. Возвращает список длиной n, состоящий из квадратов нечетных чисел в диапазоне от 0 до n.
    """
    if n is None:
        return None
    return [x ** 2 for x in range(0, n) if x % 2 > 0]


def gen_dict_double(n):
    """
    Принимает число n. Возвращает словарь длиной n, в котором ключ - это значение от 0 до n,
    а значение - удвоенное значение ключа.
    """
    if n is None:
        return None
    return dict((x, x * 2) for x in range(0, n))


def gen_dict_double_start_stop(start, stop):
    """
    Принимает числа start, stop. Возвращает словарь, в котором ключ - это значение от start до stop (не включая),
    а значение - удвоенных значение ключа.
    """
    if start is None or stop is None:
        return None
    return dict((x, x * 2) for x in range(start, stop))


def gen_dict_pow_start_stop(start, stop):
    """
    Принимает числа start, stop. Возвращает словарь, в котором ключ - это значение от start до stop (не включая),
    а значение - это квадрат ключа .
    """
    if start is None or stop is None:
        return None
    return dict((x, x ** 2) for x in range(start, stop))


def gen_dict_pow_even(n):
    """
    Принимает число n. Возвращает словарь длиной n, в котором ключ - это четное число в диапазоне от 0 до n,
    а значение - квадрат ключа.
    """
    if n is None:
        return None
    return dict((x, x ** 2) for x in range(0, n) if x % 2 == 0)


def gen_dict_pow_odd(n):
    """
    Принимает число n. Возвращает словарь длиной n, в котором ключ - это нечетное число в диапазоне от 0 до n,
    а значение - квадрат ключа.
    """
    if n is None:
        return None
    return dict((x, x ** 2) for x in range(0, n) if x % 2 > 0)

def main():
    while True:
        tn = input("Введите номер задачи Первого задания от 1 до 10 (для выхода любую другую клавишу): ")
        if tn == "1":
            help(gen_list_double)
            print("1)", gen_list_double(None))
            print("2)", gen_list_double(0))
            print("3)", gen_list_double(1))
            print("4)", gen_list_double(5))

        elif tn == "2":
            help(gen_list_double_start_stop)
            print("1)", gen_list_double_start_stop(None,None))
            print("2)", gen_list_double_start_stop(0, 0))
            print("3)", gen_list_double_start_stop(0, 5))
            print("4)", gen_list_double_start_stop(3, 8))

        elif tn == "3":
            help(gen_list_pow_start_stop)
            print("1)", gen_list_pow_start_stop(None, None))
            print("2)", gen_list_pow_start_stop(0, 0))
            print("3)", gen_list_pow_start_stop(0, 5))
            print("4)", gen_list_pow_start_stop(3, 6))

        elif tn == "4":
            help(gen_list_pow_even)
            print("1)", gen_list_pow_even(None))
            print("2)", gen_list_pow_even(0))
            print("3)", gen_list_pow_even(1))
            print("4)", gen_list_pow_even(8))

        elif tn == "5":
            help(gen_list_pow_odd)
            print("1)", gen_list_pow_odd(None))
            print("2)", gen_list_pow_odd(0))
            print("3)", gen_list_pow_odd(1))
            print("4)", gen_list_pow_odd(7))

        elif tn == "6":
            help(gen_dict_double)
            print("1)", gen_dict_double(None))
            print("2)", gen_dict_double(0))
            print("3)", gen_dict_double(1))
            print("4)", gen_dict_double(3))

        elif tn == "7":
            help(gen_dict_double_start_stop)
            print("1)", gen_dict_double_start_stop(None, None))
            print("2)", gen_dict_double_start_stop(0, 0))
            print("3)", gen_dict_double_start_stop(0, 5))
            print("4)", gen_dict_double_start_stop(3, 6))

        elif tn == "8":
            help(gen_dict_pow_start_stop)

            print("1)", gen_dict_pow_start_stop(None, None))
            print("2)", gen_dict_pow_start_stop(0, 0))
            print("3)", gen_dict_pow_start_stop(0, 5))
            print("4)", gen_dict_pow_start_stop(3, 6))

        elif tn == "9":
            help(gen_dict_pow_even)

            print("1)", gen_dict_pow_even(None))
            print("2)", gen_dict_pow_even(0))
            print("3)", gen_dict_pow_even(1))
            print("4)", gen_dict_pow_even(8))

        elif tn == "10":
            help(gen_dict_pow_odd)

            print("1)", gen_dict_pow_odd(None))
            print("2)", gen_dict_pow_odd(0))
            print("3)", gen_dict_pow_odd(1))
            print("4)", gen_dict_pow_odd(7))

        else:
            break


if __name__ == "__main__":
    main()