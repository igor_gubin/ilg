def tuple_len_first(my_tuple):
    """
    Принимает кортеж my_tuple (который создал пользователь).
    Возвращает кортеж состоящий из длины кортежа и первого элемента кортежа.
    """
    return len(my_tuple), my_tuple[0]


def tuple_len_count_first(my_tuple, num):
    """
    Принимает 2 аргумента: кортеж “ассорти” my_tuple (который создал пользователь) и число num.
    Возвращает кортеж состоящий из
    длины кортежа,
    количества чисел num в кортеже my tuple
    и первого элемента кортежа.
    Пример: my_tuple=(‘55’, ‘aa’, 66) num = 66, результат (3, 1, ‘55’).
    """
    return len(my_tuple), my_tuple.count(num), my_tuple[0]


def main():
    while True:
        tn = input("Введите номер задачи Первого задания от 1 до 2 (для выхода любую другую клавишу): ")
        if tn == "1":
            help(tuple_len_first)
            a = input("Введите произвольный кортеж: ")
            b = tuple(a.split(","))
            print(tuple_len_first(b))
        elif tn == "2":
            help(tuple_len_count_first)

            a = input("Введите произвольный кортеж: ")
            b = input("Введите число: ")
            c = tuple(a.split(","))

            print(tuple_len_count_first(c, b))
        else:
            break


if __name__ == "__main__":
    main()