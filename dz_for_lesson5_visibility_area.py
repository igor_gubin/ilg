my_var = 33


def change_global_val():
    global my_var
    my_var = 77
    return my_var


def nochange_global_val():
    my_var = 77


def my_out_fun():
    my_var = 88

    def my_in_fun():
        nonlocal my_var
        my_var += 1

    print(my_var)
    my_in_fun()
    print(my_var)


def main():
    while True:
        tn = input("Введите номер задачи от 1 до 3 (для выхода любую другую клавишу): ")
        if tn == "1":
            print(my_var)
            change_global_val()
            print(my_var)

        elif tn == "2":
            print(my_var)
            nochange_global_val()
            print(my_var)

        elif tn == "3":
            print(my_var)
            my_out_fun()
            print(my_var)
        else:
            break


if __name__ == "__main__":
    main()
