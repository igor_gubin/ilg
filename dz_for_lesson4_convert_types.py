def convert_list_to_tuple(lst, v):
    """
    Принимает 2 аргумента: список (с повторяющимися элементами) и значение для поиска.
    Возвращает кортеж (из входного списка) и найдено ли искомое значение(True | False).
    """
    return lst, v in lst


def convert_list_to_set(lst):
    """
    Принимает 1 аргумент: список (с повторяющимися элементами).
    Возвращает множество (из входного списка) и количество элементов в множестве.
    """
    return set(lst + [len(lst)])


def convert_list_to_dict(lst, v):
    """
    Принимает 2 аргумента: список (с повторяющимися элементами) и значение для поиска.
    Возвращает кортеж из словаря (из входного списка), в котором
    ключ=значение из списка, а значение=индекс этого элемента в списке,
    найдено ли искомое значение(True | False) в ключах словаря
    и найдено ли искомое значение(True | False) в значениях словаря, количество элементов в словаре.
    """
    my_dict = {}
    for i, k in enumerate(lst):
        my_dict[k] = i

    is_in_keys = True if v in my_dict.keys() else False
    is_in_vals = True if v in my_dict.values() else False

    return my_dict, is_in_keys, is_in_vals


def convert_list_to_str(lst, sep):
    """
    Принимает 2 аргумента: список (с повторяющимися элементами) и разделитель (строка).
    Возвращает строку полученную разделением элементов списка разделителем,
    а также количество разделителей в получившейся строке в квадрате.
    """
    res = sep.join(lst)
    n = res.count(sep)
    return res, n ** 2


def convert_tuple_to_list(tpl, v):
    """
    Принимает 2 аргумента: кортеж (с повторяющимися элементами) и значение для поиска.
    Возвращает список (из входного кортежа) и найдено ли искомое значение (True | False).
    """
    return [tpl, v in tpl]


def convert_tuple_to_set(tpl):
    """
    Принимает 1 аргумент: кортеж (с повторяющимися элементами).
    Возвращает множество (из входного кортежа) и количество элементов в множестве.
    """
    return {tpl, len(tpl)}


def convert_tuple_to_dict(tpl, v):
    """
    Принимает 2 аргумента: кортеж (с повторяющимися элементами) и значение для поиска.
    Возвращает кортеж из словаря (из входного кортежа), в котором ключ=значение из кортежа,
    а значение=индекс этого элемента в кортеже,
    а также найдено ли искомое значение(True | False) в ключах словаря
    и найдено ли искомое значение(True | False) в значениях словаря.
    """
    my_dict = {}
    for i, k in enumerate(tpl):
        my_dict[k] = i

    is_in_keys = True if v in my_dict.keys() else False
    is_in_vals = True if v in my_dict.values() else False

    return my_dict, is_in_keys, is_in_vals


def convert_tuple_to_str(tpl, sep):
    """
    Принимает 2 аргумента: кортеж (с повторяющимися элементами) и разделитель (строка).
    Возвращает строку полученную разделением элементов кортежа разделителем,
    а также количество разделителей в получившейся строке в степени 3.
    """
    res = sep.join(tpl)
    n = res.count(sep)
    return res, n ** 3


def convert_set_to_list(s, v):
    """
    Принимает 2 аргумента: множество и значение для поиска.
    Возвращает список (из входного множества), а также True,
    если искомое значение НЕ найдено в списке, иначе False.
    """
    return [s, False if v in s else True]


def convert_set_to_tuple(s, v):
    """
    Принимает 2 аргумента: множество и значение для поиска.
    Возвращает список (из входного множества) повторенный 2 раза, а также True,
    если искомое значение найдено в списке, иначе False.
    """
    res = [*s, *s]
    return res, v in res


def convert_set_to_dict(s, v):
    """
    Принимает 2 аргумента: множество и значение для поиска.
    Возвращает кортеж из
    словаря (из входного множества), в котором ключ=значение из множества, а значение=индекс этого
    элемента в множестве в квадрате,
    найдено ли искомое значение(True | False) в ключах словаря
    и найдено ли искомое значение(True | False) в значениях словаря,
    количество элементов в словаре.
    """
    my_dict = {}
    for i, k in enumerate(s):
        my_dict[k] = i ** 2

    is_in_keys = True if v in my_dict.keys() else False
    is_in_vals = True if v in my_dict.values() else False

    return my_dict, is_in_keys, is_in_vals, len(my_dict)


def convert_set_to_str(s, sep):
    """
    Принимает 2 аргумента: множество и разделитель (строка).
    Возвращает строку полученную разделением элементов множества разделителем,
    а также количество разделителей в получившейся строке в степени 5.
    """
    res = sep.join(s)
    n = res.count(sep)
    return res, n ** 5


def convert_dict_to_list(dic):
    """
    Принимает 1 аргумент: словарь (в котором значения повторяются).
    Возвращает список ключей, список значений, количество элементов в списке ключей,
    количество элементов в списке значений.
    """
    return list(dic.keys()) + list(dic.values()) + [len(dic.keys()), len(dic.values())]


def convert_dict_to_tuple(dic):
    """
    Принимает 1 аргумент: словарь (в котором значения повторяются).
    Возвращает кортеж ключей, множество значений, а также True, если хотя бы один ключ равен одному из значений.
    """
    kt = tuple(dic.keys())
    vs = set(dic.values())
    k_v = False
    for k in kt:
        if k in vs:
            k_v = True
            break
    return kt, vs, k_v


def convert_dict_to_set(dic):
    """
    Принимает 1 аргумент: словарь (в котором значения повторяются).
    Возвращает множество ключей, множество значений, количество элементов в множестве ключей,
    количество элементов в множестве значений.
    """
    return set(dic.keys()), set(dic.values()), len(dic.keys()), len(dic.values())


def convert_dict_to_str(dic):
    """
    Принимает 1 аргумент: словарь (в котором значения повторяются).
    Возвращает строку вида “key1=val1 | key2 = val2 | key3 = val3”.
    """
    res = ""
    for k, v in dic.items():
        if res == "":
            res = "{} = {}".format(k, v)
        else:
            res += " | {} = {}".format(k, v)
    return res


def main():
    lst = ["a", "c", "b", "a", "c", "c"]
    dic = {"a": 1, 4: 2, "c": 1, "d": 2, 2: 1, "f": 3}
    while True:
        tn = input("Введите номер задачи Первого задания от 1 до 16 (для выхода любую другую клавишу): ")
        if tn == "1":
            help(convert_list_to_tuple)
            print(convert_list_to_tuple(lst, "a"))
            print(convert_list_to_tuple(lst, "b"))
            print(convert_list_to_tuple(lst, "c"))

        elif tn == "2":
            help(convert_list_to_set)
            print(convert_list_to_set(lst))

        elif tn == "3":
            help(convert_list_to_dict)
            print(convert_list_to_dict(lst, "a"))
            print(convert_list_to_dict(lst, "b"))
            print(convert_list_to_dict(lst, "c"))
            print(convert_list_to_dict(lst, 3))
            print(convert_list_to_dict(lst, 5))
            print(convert_list_to_dict(lst, 2))

        elif tn == "4":
            help(convert_list_to_str)
            print(convert_list_to_str(lst, "-"))

        elif tn == "5":
            help(convert_tuple_to_list)
            tpl = tuple(lst)
            print(convert_tuple_to_list(tpl, "a"))
            print(convert_tuple_to_list(tpl, "b"))
            print(convert_tuple_to_list(tpl, "c"))

        elif tn == "6":
            help(convert_tuple_to_set)
            tpl = tuple(lst)
            print(convert_tuple_to_set(tpl))

        elif tn == "7":
            help(convert_tuple_to_dict)
            tpl = tuple(lst)
            print(convert_tuple_to_dict(tpl, "a"))
            print(convert_tuple_to_dict(tpl, "b"))
            print(convert_tuple_to_dict(tpl, "c"))

        elif tn == "8":
            help(convert_tuple_to_str)
            tpl = tuple(lst)
            print(convert_tuple_to_str(tpl, "-"))

        elif tn == "9":
            help(convert_set_to_list)
            s = set(lst)
            print(convert_set_to_list(s, "a"))
            print(convert_set_to_list(s, "b"))
            print(convert_set_to_list(s, "c"))

        elif tn == "10":
            help(convert_set_to_tuple)
            s = set(lst)
            print(convert_set_to_tuple(s, "a"))
            print(convert_set_to_tuple(s, "b"))
            print(convert_set_to_tuple(s, "c"))

        elif tn == "11":
            help(convert_set_to_dict)
            s = set(lst)
            print(convert_set_to_dict(s, "a"))
            print(convert_set_to_dict(s, "b"))
            print(convert_set_to_dict(s, "c"))

        elif tn == "12":
            help(convert_set_to_str)
            s = set(lst)
            print(convert_set_to_str(s, "-"))

        elif tn == "13":
            help(convert_dict_to_list)
            print(convert_dict_to_list(dic))

        elif tn == "14":
            help(convert_dict_to_tuple)
            print(convert_dict_to_tuple(dic))

        elif tn == "15":
            help(convert_dict_to_set)
            print(convert_dict_to_set(dic))

        elif tn == "16":
            help(convert_dict_to_str)
            print(convert_dict_to_str(dic))

        else:
            break


if __name__ == "__main__":
    main()
