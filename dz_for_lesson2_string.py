import random


def as_int(sv):
    res = 0
    try:
        res = int(sv)
    except:
        res = None
    return res


def print_first_last_mid(s):
    """
    Принимает строку. Выводит первый, последний и средний, если он есть.
    """
    lns = len(s)
    m = None if lns % 2 == 0 else lns // 2
    print("Первый {}; Последний {}; Средний {}".format(s[0], s[-1], s[m] if m is not None else None))


def print_symbols_if(s):
    """
    Принимает строку. Возвращает первые три символа и последние три символа, если длина строки больше 5.
    Иначе выводит первый символ столько раз, какова длина строки.
    """
    res = ""
    lns = len(s)
    if lns > 5:
        res = f"{s[0:3]}..{s[-3:lns]}"
    elif lns > 0:
        res = s[0] * lns
    return res


def print_nth_symbols(s, sn):
    """
    Принимает строку и натуральное число n. Вывести символы с индексом n, n*2, n*3 и так далее.
    """
    n = as_int(sn)
    if n is not None:
        b = 0 if n - 1 < 0 else n - 1
        print(s[b::n])
    else:
        print("-")


def print_after_sym(s, sym):
    """
    Принимает строку произвольной длины и строку единичной длины sym.
    Выводит общее количество символов после которых следует символ sym.
    """

    v1 = s.replace(sym, "")
    if v1 == s:
        print(0)
    else:
        print(len(v1))


def print_plus_minus_after0(s):
    """
    Принимает строку. Выводит общее количество символов '+' и '-' в ней.
    А также сколько таких символов, после которых следует цифра ноль.
    """
    v1 = str(s).count("+") + str(s).count("-")
    v2 = str(s).count("+0") + str(s).count("-0")
    print("Total: {}; Previous to zero count: {}".format(v1, v2))


def which_first_one_or_another(s, one_sym, another_sym):
    """
    Принимает строку произвольной длины и две строки единичной длины one_sym, another_sym.
    Определите и напечатайте, какой символ в ней встречается раньше:
    one_sym или another_sym.
    Если какого-то из символов нет, вывести сообщение вида “Couldn’t find symbol: ‘t’”.
    """
    pos1 = s.find(one_sym)
    pos2 = s.find(another_sym)
    if pos1 < 0 and pos2 < 0:
        print("Couldn't find symbols: '{}', '{}'".format(one_sym, another_sym))
    elif pos1 < 0:
        print("Couldn't find symbol: '{}'".format(one_sym))
    elif pos2 < 0:
        print("Couldn't find symbol: '{}'".format(another_sym))
    else:
        print(one_sym if pos1 < pos2 else another_sym)


def modify_str_if_len_gt(s, sn):
    """
    Принимает строку и целое положительное число n.
    Если длина строки больше n, то оставляет в строке только первые 6 символов,
    иначе дополнить строку символами 'o' до длины 12.
    """
    res = ""
    n = as_int(sn)
    lns = len(s)
    if lns > n:
        res = s[0:6]
    else:
        res = s + "o" * (12 - lns)
    return res


def replace_even_odd_symbols(s):
    """
    Принимает строку. Возвращает результат замены каждого четного символа или на 'a',
    если символ не равен 'a' или 'b', или на 'c' в противном случае.
    """
    res = ""
    m = len(s) / 2
    for i, c in enumerate(s):
        if i % 2:
            res += "a" if c != "a" else "bc"[random.randint(0, 1)]
        else:
            res += c
    return res


def count_numbers(s):
    """
    Принимает строку. Возвращает количество цифр в данной строке.
    """
    res = 0
    for ch in str(s):
        v = as_int(ch)
        if v is not None:
            res += 1
    return res


def check_abc(s):
    """
    Принимает строку. Если строка содержит только символы 'a', 'b', 'c', то вернуть True, иначе False.
    """
    res = True
    for ch in str(s):
        if ch in "abc":
            continue
        else:
            res = False
            break
    return res


def count_substr(origin, substr):
    """
    Принимает две строки origin и substr. Возвращает количество вхождений substr в строку origin.
    """
    return str(origin).count(substr)


def sub_numbers_in_str(s):
    """
    Принимает строку. Возвращает сумму имеющихся в ней цифр.
    """
    res = 0
    for ch in str(s):
        v = as_int(ch)
        if v is not None:
            res += v
    return res


def symbols_a_b_caseup(s):
    """
    Принимает строку. Возвращает строку, где все символы 'a' и 'b' # заменены на 'A' и 'B' соответственно.
    """
    return str(s).replace("a", "A").replace("b", "B")


def check_substr(s1, s2):
    """
    Принимает две строки. Если меньшая по длине строка содержится в большей, то возвращает True, иначе False.
    """
    if len(s1) > len(s2):
        return s2 in s1
    else:
        return s1 in s2


def remove_question(s):
    """
    Принимает строку, возвращает результат удаления из строки символа "?"
    """
    return str(s).replace("?", "")


def count_words_by_symb(s, sep):
    """
    Принимает две строки. Возвращает количество слов в первой строке разделенных символами второй.
    """
    res = 0
    tmp = str(s).split(sep)
    for w in tmp:
        if len(w) > 0:
            res += 1
    return res


def convert_to_roman(s):
    """
    Принимает строку. В строке записано десятичное число. возвращает строку, где данное число записано римскими цифрами.
    https://ru.wikipedia.org/wiki/%D0%A0%D0%B8%D0%BC%D1%81%D0%BA%D0%B8%D0%B5_%D1%86%D0%B8%D1%84%D1%80%D1%8B
    """
    ones = ["", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"]
    tens = ["", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"]
    hunds = ["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"]
    thous = ["", "M", "MM", "MMM", "CIƆIƆƆ", "IƆƆ", "IƆƆCIƆ", "IƆƆCIƆCIƆ", "IƆƆCIƆCIƆCIƆ", "CIƆCCIƆƆ"]
    tens_thous = ["", "CCIƆƆ", "CCIƆƆCCIƆƆ", "CCIƆƆCCIƆƆCCIƆƆ", "CCIƆƆIƆƆƆ", "IƆƆƆ", "IƆƆƆCCIƆƆ", "IƆƆƆCCIƆƆCCIƆƆ",
                  "IƆƆƆCCIƆƆCCIƆƆCCIƆƆ", "CCIƆƆCCCIƆƆƆ"]
    hunds_thous = ["", "CCCIƆƆƆ", "CCCIƆƆƆCCCIƆƆƆ", "CCCIƆƆƆCCCIƆƆƆCCCIƆƆƆ", "CCCIƆƆƆIƆƆƆƆ", "IƆƆƆƆ", "IƆƆƆƆCCCIƆƆƆ",
                   "IƆƆƆƆCCCIƆƆƆCCCIƆƆƆ", "???", "???"]

    v = as_int(s)

    ht = hunds_thous[v // 100000 % 10]
    tt = tens_thous[v // 10000 % 10]
    t = thous[v // 1000 % 10]
    h = hunds[v // 100 % 10]
    te = tens[v // 10 % 10]
    o = ones[v % 10]

    return ht + tt + t + h + te + o


def check_email(s):
    """
    Принимает строку. Определяет, является ли она корректным email и возвращает True, если корректный, иначе False.
    (наличие символа @ и точки, наличие не менее двух символов после последней точки и т.д.).
    """
    res = True
    l1 = str(s).split("@")
    if len(l1) == 2:
        i = 0
        for w1 in l1:
            l2 = w1.split(".")
            if i > 0:
                if len(l2) < 2 or len(l2[-1]) < 2:
                    res = False
                    break
            for w2 in l2:
                if len(w2) == 0:
                    res = False
                    break
                for ch in w2:
                    if "a" <= ch <= "z" or "A" <= ch <= "Z" or "0" <= ch <= "9" or ch in "_-":
                        continue
                    else:
                        res = False
                        break
            else:
                i += 1
                continue
            break
    else:
        res = False

    return res


def split_triples_with_space(sn):
    """
    Принимает натуральное число. Возвращает строку, в которой тройки цифр этого числа разделены пробелом,
    начиная с правого конца.
    """
    n = as_int(sn)
    if n is None or n <= 0:
        return None  # не натуральное число
    tmp = str(n)
    res = ""
    lnn = len(tmp)
    for i in range(0, lnn):
        res = tmp[lnn - (i + 1)] + res
        if (i + 1) % 3 == 0:
            res = " " + res
    return res


def split_symb_with_space(s):
    """
    Принимает строку. Возвращает строку полученную из исходной вставкой после каждого символа пробела.
    """
    res = ""
    for i in range(0, len(s)):
        res += s[i] + " "
    return res


def check_alfabet_order(s):
    """
    Принимает строку. Если символы в ней упорядочены по алфавиту, то выводит ‘YES’,
    иначе - первый символ, нарушающий алфавитный порядок.
    """
    ch = ""
    ss = sorted(s)
    for i in range(0, len(s)):
        if s[i] != ss[i]:
            ch = s[i]
            break
    return "YES" if ch == "" else ch


def compare_strings(s1, s2):
    """
    Принимает две строки. Сравнивает эти две строки. Возвращает True при равенстве, иначе False.
    """
    return s1 == s2


def main():
    while True:
        tn = input("Введите номер задачи Первого задания от 1 до 23 (для выхода любую другую клавишу): ")
        if tn == "1":
            help(print_first_last_mid)
            a = input("Введите любую строку: ")
            print_first_last_mid(a)
        elif tn == "2":
            help(print_symbols_if)
            a = input("Введите любую строку: ")
            print(print_symbols_if(a))
        elif tn == "3":
            help(print_nth_symbols)
            a = input("Введите строку: ")
            b = input("Введите номер: ")
            print_nth_symbols(a, b)
        elif tn == "4":
            help(print_after_sym)
            a = input("Введите строку: ")
            b = input("Введите символ: ")
            print_after_sym(a, b)
        elif tn == "5":
            help(print_plus_minus_after0)
            a = input("Введите строку: ")
            print_plus_minus_after0(a)
        elif tn == "6":
            help(which_first_one_or_another)
            a = input("Введите строку: ")
            b = input("Введите первый символ: ")
            c = input("Введите второй символ: ")
            which_first_one_or_another(a, b, c)
        elif tn == "7":
            help(modify_str_if_len_gt)
            a = input("Введите строку: ")
            b = input("Введите натуральное число: ")
            print(modify_str_if_len_gt(a, b))
        elif tn == "8":
            help(replace_even_odd_symbols)
            a = input("Введите строку: ")
            print(replace_even_odd_symbols(a))
        elif tn == "9":
            help(count_numbers)
            a = input("Введите строку: ")
            print(count_numbers(a))
        elif tn == "10":
            help(check_abc)
            a = input("Введите строку: ")
            print(check_abc(a))
        elif tn == "11":
            help(count_substr)
            a = input("Введите первую строку: ")
            b = input("Введите вторую строку: ")
            print(count_substr(a, b))
        elif tn == "12":
            help(sub_numbers_in_str)
            a = input("Введите строку: ")
            print(sub_numbers_in_str(a))
        elif tn == "13":
            help(symbols_a_b_caseup)
            a = input("Введите первую строку: ")
            print(symbols_a_b_caseup(a))
        elif tn == "14":
            help(check_substr)
            a = input("Введите первую строку: ")
            b = input("Введите вторую строку: ")
            print(check_substr(a, b))
        elif tn == "15":
            help(remove_question)
            a = input("Введите строку: ")
            print(remove_question(a))
        elif tn == "16":
            print("1 2 4 8 7 5 / 3 6 9")
        elif tn == "17":
            help(count_words_by_symb)
            a = input("Введите строку состоящую из слов: ")
            b = input("Введите разделитель: ")
            print(count_words_by_symb(a, b))
        elif tn == "18":
            help(convert_to_roman)
            a = input("Введите натуральное число: ")
            print(convert_to_roman(a))
        elif tn == "19":
            help(check_email)
            a = input("Введите email: ")
            print(check_email(a))
        elif tn == "20":
            help(split_triples_with_space)
            a = input("Введите натуральное число: ")
            print(split_triples_with_space(a))
        elif tn == "21":
            help(split_symb_with_space)
            a = input("Введите строку: ")
            print(split_symb_with_space(a))
        elif tn == "22":
            help(check_alfabet_order)
            a = input("Введите строку: ")
            print(check_alfabet_order(a))
        elif tn == "23":
            help(compare_strings)
            a = input("Введите первую строку для сравнения: ")
            b = input("Введите первую строку для сравнения: ")
            print(compare_strings(a, b))
        else:
            break


if __name__ == "__main__":
    main()
