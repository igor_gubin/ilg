def as_float(sv):
    res = 0
    try:
        res = float(sv)
    except:
        res = None
    return res


def main():
    while True:
        tn = input("Введите номер задачи от 1 до 4 (для выхода любую другую клавишу): ")
        if tn == "1":
            print("lambda делит аргумент a на аргумент b и выводит результат")
            fn1 = lambda a, b: print("?" \
                                         if as_float(a) is None or as_float(a) is None else "infinity" if b == 0 \
                else str(a / b))
            fn1(1, 0)
            fn1(5, 7)
            fn1(77.89, 15.56)
            fn1(0, 15.56)

        elif tn == "2":
            print("lambda суммирует аргументы a, b и c и выводит результат")
            fn2 = lambda a, b, c: print(str(as_float(a) + as_float(b) + as_float(c)) \
                                            if not isinstance(a, str) and not isinstance(b, str) and not isinstance(c,
                                                                                                                    str) \
                                            else "?")
            fn2(1, 1, 1)
            fn2(2, -1, 3)
            fn2(0, 0, 0)
            fn2(0, "0", 0)

        elif tn == "3":
            print("lambda перемножает аргументы a, b и c и выводит результат")
            fn3 = lambda a, b, c: print(str(as_float(a) * as_float(b) * as_float(c)) \
                                            if not isinstance(a, str) and not isinstance(b, str) and not isinstance(c,
                                                                                                                    str) \
                                            else "?")
            fn3(1, 1, 1)
            fn3(2, -1, 3)
            fn3(0, 0, 0)
            fn3(0, "0", 0)

        elif tn == "4":
            print("lambda возводит в квадрат переданный аргумент возвращает результат")
            fn4 = lambda a: as_float(a) ** 2 if not isinstance(a, str) and as_float(a) is not None else None
            print(fn4(0))
            print(fn4("0"))
            print(fn4(25.45))
            print(fn4(8))

        else:
            break


if __name__ == "__main__":
    main()
