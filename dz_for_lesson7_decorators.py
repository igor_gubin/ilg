def add_hello_bye_dec(func):
    # Декоратор. Параметр: функция func без аргументов
    def add_hello_bye():
        """
        Внутренняя функция без аргументов.
        Возвращает строку состоящую из "Привет!\n" + <результат func> + "\nПока!"
        """
        print("Привет!")
        func()
        print("Пока!")

    return add_hello_bye


@add_hello_bye_dec
def normal_fun():
    """
    Возвращает строку “Я абсолютно нормальная функция”
    """
    print("Я абсолютно нормальная функция")


def add_before_after_dec(func):
    """
    Декоратор. Параметр: функция func без аргументов.
    """

    def add_before_after():
        """
        Возвращает строку состоящую из: "Before...\n" + <результат func> + "\nAfter..."
        """
        return f"Before...\n" + str(func()) + "\nAfter..."

    return add_before_after


@add_before_after_dec
def basic_fun():
    """
    Возвращает строку “Basic function”.
    """
    return "Basic function"


def opinionated_name_age_dec(func):
    """
    Декоратор. Параметры: функция func имеет аргументы name, age
    """

    def opinionated_name_age(name, age):
        print(f"Я знаком с {name}! А мне не нравится возраст {age}...")

    return opinionated_name_age


@opinionated_name_age_dec
def hello_name_age(name, age):
    """
    Печатает строку “Привет name! age - это еще молодость!
    """
    print(f"Привет {name}! {age} - это еще молодость!")


# Декораторы для методов
def double_val_dec(func):
    def double_val(self, seed_count):
        """
        Принимает self и seed_count.
        Удваивает количество семечек (seed_count *= 2) и вызывает функцию func с удвоенным количеством семечек.
        """
        func(self, seed_count * 2)

    return double_val


class Humster:
    def __init__(self, moniker, seeds_count=0):
        self.moniker = moniker
        self.seeds_count = int(seeds_count)

    @double_val_dec
    def add_seeds(self, seed_count):
        self.seeds_count += int(seed_count)

    def print_seed_count(self):
        print(self.seeds_count)


def main():
    while True:
        tn = input("Введите номер задачи от 1 до 4 (для выхода любую другую клавишу): ")
        if tn == "1":
            normal_fun()
        elif tn == "2":
            print(basic_fun())
        elif tn == "3":
            name = input("Введите имя: ")
            age = int(input("Введите возраст: "))
            hello_name_age(name, age)
        elif tn == "4":
            moniker = input("Введите кличку хомяка: ")
            humster = Humster(moniker)
            seed_count = int(input("Введите запас семечек: "))
            if seed_count < 0:
                seed_count *= -1
            humster.add_seeds(seed_count)
            humster.print_seed_count()
        else:
            break


if __name__ == "__main__":
    main()
