def print_soap_price(soap, *prices):
    """
    Принимает 2 аргумента: название мыла (строка) и # неопределенное количество цен (*args).
    Выводит вначале название мыла, а потом все цены на него.
    """
    all_pices = ", ".join([str(p) for p in prices])
    print(f"Название мыла: {soap}; Все цены: {all_pices}")


def print_student_marks_by_subject(fname, **acperf):
    """
    Принимает 2 аргумента: имя студента и именованные аргументы с оценками по различным предметам.
    Выводит имя студента, а затем предмет и оценку (может быть несколько, если курс состоял из нескольких частей).
    """
    for k in acperf:
        grades = acperf[k]
        grades_str = str(grades)
        if not str(grades).isdigit():
            grades_str = ", ".join([str(g) for g in list(acperf[k])])
        print(f"Студент: {fname}; Предмет: {k}; Оценки: {grades_str}")


def main():
    while True:
        tn = input("Введите номер задачи от 1 до 2 (для выхода любую другую клавишу): ")
        if tn == "1":
            print_soap_price("Dove", 10, 50)
            print_soap_price("Мылко", 456, 876, 555)
            pass
        elif tn == "2":
            print_student_marks_by_subject("Вася", math = 5, biology = (3, 4), magic = (4, 5, 5))
        else:
            break


if __name__ == "__main__":
    main()