def natural_sum(n):
    """
    Вычисляет сумму натуральных чисел от 1 до n.
    Если n == 1, то сумма равна 1.
    Иначе сумма чисел от 1 до n равна сумме чисел от 1 до n - 1,
    которую можно вычислить при помощи рекурсии плюс число n.
    """
    if isinstance(n, int) and n > 0:
        return 1 if n == 1 else n + natural_sum(n - 1)
    return None

def main():
    help(natural_sum)
    print(natural_sum(0))
    print(natural_sum(1))
    print(natural_sum(4))
    print(natural_sum(8))

if __name__ == "__main__":
    main()