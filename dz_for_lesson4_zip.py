from itertools import zip_longest


def zip_names(lfn, sln):
    """
    Принимает 2 аргумента: список с именами и множество с фамилиями.
    Возвращает список с парами значений из каждого аргумента.
    """
    return list(zip(lfn, sln))


def zip_colour_shape(colors, shapes):
    """
    Принимает 2 аргумента: список с цветами(зеленый, красный) и кортеж с формами (квадрат, круг).
    Возвращает список с парами значений из каждого аргумента.
    """
    return list(zip(colors, shapes))


def zip_car_year(cars, years):
    """
    Принимает 2 аргумента: список с машинами и список с годами производства.
    Возвращает список с парами значений из каждого аргумента, если один список больше другого, то
    заполнить недостающие элементы строкой “???”.
    """
    return list(zip_longest(cars, years, fillvalue="???"))

def main():
    while True:
        tn = input("Введите номер задачи Первого задания от 1 до 3 (для выхода любую другую клавишу): ")
        if tn == "1":
            help(zip_names)
            lfn = ["Alex", "Lary", "Theodor"]
            sln = {"Pascal", "Tesla", "Landau"}
            print(zip_names(lfn, sln))
            sln.remove("Landau")
            print(zip_names(lfn, sln))
            sln.clear()
            print(zip_names(lfn, sln))

        elif tn == "2":
            help(zip_colour_shape)
            colors = ["зеленый", "красный"]
            shapes = ("квадрат", "круг")
            print(zip_colour_shape(colors, shapes))
            del colors[0]
            print(zip_colour_shape(colors, shapes))
            del colors[0]
            print(zip_colour_shape(colors, shapes))

        elif tn == "3":
            help(zip_car_year)

            cars = ["toyota", "cia", "volvo", "audi"]
            years = [2001, 2005, 1999, 2019]

            print(zip_car_year(cars, years))

            del cars[0]
            print(zip_car_year(cars, years))
            del cars[0]
            print(zip_car_year(cars, years))
            del cars[0]
            print(zip_car_year(cars, years))
            del cars[0]
            print(zip_car_year(cars, years))

            del years[0]
            print(zip_car_year(cars, years))
            del years[0]
            print(zip_car_year(cars, years))
            del years[0]
            print(zip_car_year(cars, years))
            del years[0]
            print(zip_car_year(cars, years))
            pass
        else:
            break

if __name__ == "__main__":
    main()