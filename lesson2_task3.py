def triple_sting_print_len(sv):
    """
    Принимает строку и выводит ее три раза через запятую и показывает количество символов в ней.
    """
    v = str(sv)
    res = v + ", " + v + ", " + v
    print("Исходная строка \"", v, "\" len: ", len(v), sep="")
    print("Результат \"", res, "\" len: ", len(res), sep="")
    return None


def print_3th_symbols(sv):
    """
    Принимает строку, выводит каждый третий символ из нее.
    """
    v = str(sv);
    i = 1
    for ch in v:
        if i % 3 == 0:
            print(ch, end="")
        i += 1
    print()
    return None


def print_diff_count(sv1, sv2):
    """
    Принимает две строки, выводит большую по длине строку столько раз, насколько символов отличаются строки.
    """
    v1 = str(sv1)
    v2 = str(sv2)

    dif = len(v1) - len(v2)
    if dif > 0:
        print(v1 * dif)
    elif dif < 0:
        print(v2 * (dif * -1))
    return None


def modify_str_if_begin_abc(sv):
    """
    Принимает строку. Если она начинается со строки 'abc', то заменяет ее на 'www',
    иначе добавляет в конец строки 'zzz' и возвращает результат.
    """
    v = str(sv)

    res = ""
    if len(v) >= 3:
        if v[0: 3] == "abc":
            res = "www" + v[3:]
        else:
            res = v + "zzz"
    else:
        res = v
    return res


def replace_word(sv):
    """
    Принимает строку, возвращает строку, в которой вхождения 'word' заменены на 'letter'.
    """

    return str(sv).replace("word", "letter")


def count_aba(sv):
    """
    Принимает строку. Возвращает количество вхождений подстроки 'aba' в строку.
    """
    v = str(sv)
    found_index = 1
    count = 0
    while found_index > -1:
        found_index = v.find("aba")
        if found_index >= 0:
            count += 1
            v = v[found_index + 1:]
    return count


def remove_exclamation(sv):
    """
    Удаляет в строке все символы "!" и возвращает результат.
    """
    return str(sv).replace("!", "")


def count_words_by_spaces(sv):
    """
    Принимает строку, состоящую из слов, разделенных пробелами, возвращает количество слов в строке.
    """
    state = 0  # 0 - начало или пробел; 1 - слово
    count = 0  # счетчик слов
    v = str(sv)
    for ch in v:
        if ch == " " or ch == "\t" or ch == "\r" or ch == "\n":
            if state != 0:
                state = 0
        else:
            if state == 0:
                state = 1
                count += 1
    return count


if __name__ == "__main__":
    while True:
        tn = input("Введите номер задачи Первого задания от 1 до 8 (для выхода любую другую клавишу): ")
        if tn == "1":
            print("Задача 3.1")
            help(triple_sting_print_len)

            a = input("Введите любую строку: ")
            triple_sting_print_len(a)
        elif tn == "2":
            print("Задача 3.2")
            help(print_3th_symbols)

            a = input("Введите любую строку: ")
            print_3th_symbols(a)
        elif tn == "3":
            print("Задача 3.1")
            help(print_diff_count)

            a = input("Введите первую строку: ")
            b = input("Введите вторую строку: ")
            print_diff_count(a, b)
        elif tn == "4":
            print("Задача 3.4")
            help(modify_str_if_begin_abc)

            a = input("Введите любую строку: ")
            print(modify_str_if_begin_abc(a))
        elif tn == "5":
            print("Задача 3.5")
            help(replace_word)

            a = input("Введите любую строку: ")
            print(replace_word(a))
        elif tn == "6":
            print("Задача 3.6")
            help(count_aba)

            a = input("Введите любую строку: ")
            print(count_aba(a))
        elif tn == "7":
            print("Задача 3.7")
            help(remove_exclamation)

            a = input("Введите любую строку: ")
            print(remove_exclamation(a))
        elif tn == "8":
            print("Задача 3.8")
            help(count_words_by_spaces)

            a = input("Введите любую строку: ")
            print(count_words_by_spaces(a))
        else:
            break
