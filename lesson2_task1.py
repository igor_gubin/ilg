def as_float(sv):
    res = 0
    try:
        res = float(sv)
    except:
        res = None
    return res


def season(sm):
    """
    Возвращает время года (зима, весна, лето или осень) по заданному месяцу (от 1 до 12).
    """
    if not sm.isdigit():
        return "Значение должно быть задано числом в диаппазоне от 1 до 12"

    m = int(sm)
    res = ""
    if m < 1 or m > 12:
        return "Значение должно быть задано числом в диаппазоне от 1 до 12"
    if 1 <= m <= 2 or m == 12:
        res = "зима"
    elif 3 <= m <= 5:
        res = "весна"
    elif 6 <= m <= 8:
        res = "лето"
    elif 9 <= m <= 11:
        res = "осень"
    return res


def swap(a, b):
    """
    Меняет значения переменных местами
    """
    return b, a


def check_numbers_by_5(sv1, sv2, sv3):
    """
    Принимает 3 числа если ровно 2 из них меньше 5 возвращает True иначе False
    """

    v1 = as_float(str(sv1));
    v2 = as_float(str(sv2));
    v3 = as_float(str(sv3));

    tst = (1 if (v1 is not None) and v1 < 5 else 0) \
          + (2 if (v2 is not None) and v2 < 5 else 0) \
          + (4 if (v3 is not None) and v3 < 5 else 0)
    return tst == 3 or tst == 5 or tst == 6


def check_equal(sv1, sv2, sv3):
    """
    Принимает 3 числа, если среди них есть одинаковые, возвращает True иначе False.
    """
    v1 = as_float(str(sv1));
    v2 = as_float(str(sv2));
    v3 = as_float(str(sv3));

    return v1 == v2 or v1 == v3 or v2 == v3


# 5. Функция count_positive . Принимает 3 числа. Вернуть количество положительных чисел среди них.
def count_positive(sv1, sv2, sv3):
    v1 = as_float(str(sv1));
    v2 = as_float(str(sv2));
    v3 = as_float(str(sv3));

    res = (1 if v1 >= 0 else 0) + (1 if v2 >= 0 else 0) + (1 if v3 >= 0 else 0)
    return res


# 6. Функция sum_div_5 .

def sum_div_5(sv1, sv2, sv3):
    """
    Принимает 3 числа, возврашает сумму тех чисел, которые делятся на 5.
    """
    v1 = as_float(str(sv1));
    v2 = as_float(str(sv2));
    v3 = as_float(str(sv3));

    res = (v1 if v1 % 5 == 0 else 0) + (v2 if v2 % 5 == 0 else 0) + (v3 if v3 % 5 == 0 else 0)
    return res


def if_7_yes_no(sv):
    """
    Принимает число. Если оно меньше 7, то возвращает “Yes”, если больше 10,
    то вернуть “No”, если равно 9, то вернуть “Error”

    :param sv:
    :return:
    """
    v = as_float(str(sv));

    return "Yes" if v < 7 else ("No" if v > 10 else ("Error" if v == 9 else ""))


def check_plus_last_num(sv):
    """
    Принимает двузначное число, возвращает результат вычисления выражения: число + (последняя цифра числа).
    """
    tmp = str(sv)
    v = as_float(tmp)
    res = None
    if v is not None and len(tmp.lstrip("-+")) == 2:
        res = v + float(tmp[-1])
    return res

if __name__ == "__main__":
    while True:
        tn = input("Введите номер задачи Первого задания от 1 до 8 (для выхода любую другую клавишу): ")
        if tn == "1":
            print("Задача 1.1")
            help(season)

            mStr = input("Введите порядковый номер месяца от 1 до 12: ")
            print(season(mStr))
        elif tn == "2":
            print("Задача 1.2")
            help(swap)

            a = input("Ведите первое значение: ")
            b = input("Ведите второе значение: ")
            a, b = swap(a, b)
            print("a:", a, "b:", b)

        elif tn == "3":
            print("Задача 1.3")
            help(check_numbers_by_5)

            a = input("Ведите первое число: ")
            b = input("Ведите второе число: ")
            c = input("Ведите третье число: ")

            print(check_numbers_by_5(a, b, c))

        elif tn == "4":
            print("Задача 1.4")
            help(check_equal)

            a = input("Ведите первое число: ")
            b = input("Ведите второе число: ")
            c = input("Ведите третье число: ")

            print(check_equal(a, b, c))

        elif tn == "5":
            print("Задача 1.5")
            help(count_positive)

            a = input("Ведите первое число: ")
            b = input("Ведите второе число: ")
            c = input("Ведите третье число: ")

            print(count_positive(a, b, c))

        elif tn == "6":
            print("Задача 1.6")
            help(sum_div_5)

            a = input("Ведите первое число: ")
            b = input("Ведите второе число: ")
            c = input("Ведите третье число: ")

            tmp = sum_div_5(a, b, c)
            print(tmp if tmp > 0 else "Error")
        elif tn == "7":
            print("Задача 1.7")
            help(if_7_yes_no)

            a = input("Ведите число: ")
            print(if_7_yes_no(a))

        elif tn == "8":
            print("Задача 1.8:")
            help(check_plus_last_num)

            a = input("Ведите двузначное число: ")
            print(check_plus_last_num(a))
        else:
            break;
