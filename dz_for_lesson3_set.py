def set_print_info_for_2(my_set_left, my_set_right):
    """
    Принимает 2 аргумента: множества “ассорти” my_set_left и my_set_right (которые создал пользователь).
    Выводит информацию о: равенстве множеств, имеют ли они общие элементы,
    является ли my_set_left подмножеством my_set_right и наоборот.
    """
    print("1) Равенство:", my_set_left == my_set_right)
    print("2) Пересечение:", my_set_left.intersection(my_set_right))
    print("3) 1 подмножество 2:", my_set_left.issubset(my_set_right))
    print("4) 2 подмножество 1:", my_set_left.issuperset(my_set_right))


def set_print_info_for_3(my_set_left, my_set_mid, my_set_right):
    """
    Принимает 3 аргумента: множества “ассорти” my_set_left, my_set_mid, # my_set_right (которые создал пользователь).
    Выводит информацию о: равенстве множеств, имеют ли они общие элементы, являются ли они подмножествами друг друга.
    """
    print("1) Равенство:", my_set_left == my_set_mid == my_set_right)
    print("2) Пересечение:", my_set_left.intersection(my_set_mid).intersection(my_set_right))
    print("3) 1 подмножество 2:", my_set_left.issubset(my_set_mid))
    print("4) 2 подмножество 1:", my_set_left.issuperset(my_set_mid))
    print("3) 1 подмножество 3:", my_set_left.issubset(my_set_right))
    print("4) 3 подмножество 1:", my_set_left.issuperset(my_set_right))
    print("3) 2 подмножество 3:", my_set_mid.issubset(my_set_right))
    print("4) 3 подмножество 2:", my_set_mid.issuperset(my_set_right))


def main():
    while True:
        tn = input("Введите номер задачи Первого задания от 1 до 2 (для выхода любую другую клавишу): ")
        if tn == "1":
            help(set_print_info_for_2)

            a = input("Введите элементы множества1': ")
            b = input("Введите элементы множества2': ")
            sa = set(a.split(","))
            sb = set(b.split(","))
            set_print_info_for_2(sa, sb)
        elif tn == "2":
            help(set_print_info_for_3)

            a = input("Введите элементы множества1': ")
            b = input("Введите элементы множества2': ")
            c = input("Введите элементы множества3': ")
            sa = set(a.split(","))
            sb = set(b.split(","))
            sc = set(c.split(","))

            set_print_info_for_3(sa, sb, sc)
        else:
            break


if __name__ == "__main__":
    main()
