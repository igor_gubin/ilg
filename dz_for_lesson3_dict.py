def get_translation_by_word(ru_word, ru):
    """
    Принимает 2 аргумента: ru-eng словарь содержащий ru_word: [eng_1, eng_2 ...] и слово для поиска в словаре (ru).
    Возвращает все варианты переводов, если такое слово есть в словаре,
    если нет, то ‘Can’t find Russian word: {word}’.
    """
    res = ru_word.get(ru)
    if res is not None:
        return res
    else:
        return "Can’t find Russian word: {}".format(ru)


def get_words_by_translation(ru_word, eng):
    """
    Принимает 2 аргумента: ru-eng словарь содержащий ru_word: [eng_1, eng_2 ...] и слово для поиска в словаре (eng).
    Возвращает список всех вариантов переводов (ru), если такое слово есть в словаре (eng),
    если нет, то ‘Can’t find English word: {word}’.
    """

    for k, v in ru_word.items():
        if eng in v:
            return k
    return "Can’t find English word: {}".format(eng)


def main():
    ru_eng = {"сильный": ["strong", "powerful", "heavy"], "молодец": ["brave", "fine fellow"]}
    while True:
        tn = input("Введите номер задачи Первого задания от 1 до 2 (для выхода любую другую клавишу): ")
        if tn == "1":
            help(get_translation_by_word)
            a = input("Введите слово для поиска на русском: ")
            print(get_translation_by_word(ru_eng, a.lstrip().rstrip()))
        elif tn == "2":
            help(get_words_by_translation)
            a = input("Введите слово для поиска на английском: ")
            print(get_words_by_translation(ru_eng, a.lstrip().rstrip()))
        else:
            break


if __name__ == "__main__":
    main()
