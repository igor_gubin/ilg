import random


def as_int(sv):
    res = 0
    try:
        res = int(sv)
    except:
        res = None
    return res


def as_float(sv):
    res = 0
    try:
        res = float(sv)
    except:
        res = None
    return res


def print_hi(sn):
    """
    Принимает число n. Выводит на экран n раз фразу "Hi, friend!"
    """
    n = as_int(sn)
    print("Hi, friend!" * n)
    return None


def print_numbers(sv1, sv2, sv3):
    """
    Принимает 3 числа: start (начальное число), end (последнее число) и step (шаг).
    Выводит на экран числа от start (включительно) до end (включительно) с шагом step.
    """
    start = as_int(sv1)
    end = as_int(sv2)
    step = as_int(sv3)
    for i in range(start, end, step):
        if i == start:
            print(i, end="")
        else:
            print(f", {i}", end="")
    if i + step >= end:
        print(f", {end}")
    else:
        print()
    return None


def print_count_down_100():
    """
    Выводит на экран числа 100, 96, 92, ... до последнего положительного включительно.
    """
    for i in range(100, 0, -4):
        if i == 100:
            print(i, end="")
        else:
            print(f", {i}", end="")
    print()
    return None


def sum_1_112_3():
    """
    Возвращает сумму 1+4+7+11+...+112.
    """
    sum = 0
    for i in range(1, 112 + 1, 3):
        sum += i
    return sum


def print_mul_table(sn):
    """
    Принимает число n. Выводит на экран таблицу умножения для числа n.
    """
    n = as_int(sn)
    for i in range(1, 10):
        print(f"{n} X {i} = {n * i}")
    return None


def count_div(sv1, sv2, sv3):
    """
    Принимает целые числа a, b и d.
    Возвращает количество целых чисел от a до b включительно, которые делятся на d без остатка.
    """
    count = 0
    a = as_int(sv1)
    b = as_int(sv2)
    d = as_int(sv3)

    for i in range(a, b + 1):
        if i % d == 0:
            count += 1
    return count


def count_user_even_numbers():
    """
    Пользователь вводит ненулевые целые числа до тех пор, пока не введет ноль.
    Возвращает количество четных чисел, которые он ввел.
    """
    count = 0
    si = ""
    while si != "0":
        si = input("Введите ненулевое число, для выхода нажмите 0:")
        i = as_int(si)
        if i is None:
            continue
        if i % 2 == 0:
            count += 1
    return count


def print_sum_4num(sn):
    """
    Принимает целое число n. Возвращает четырехзначные числа, сумма цифр которых равна n.
    """
    v = as_int(sn)
    if v is None:
        return None
    res = set()
    start_out = False
    for i in range(1000, 10000, 1):
        t = str(i)
        sum = 0
        for ch in t:
            sum += as_int(ch)
        if sum == v:
            res.add(i)
            if not start_out:
                start_out = True

    return res


def count_odd_num(sn):
    """
    Принимает натуральное число. Возвращает количество нечетных цифр в этом числе.
    """
    v = as_int(sn)
    if v is None or v <= 0:
        return None
    cnt = 0
    t = str(v)
    for ch in t:
        chv = as_int(ch)
        if chv % 2 != 0:
            cnt += 1
    return cnt


def count_num_between(sn, smin, smax):
    """
    Принимает числа n, min и max. Возвращает количество нечетных цифр в этом числе,
    которые больше min, но меньше max.
    """

    n = as_int(sn)
    min = as_int(smin)
    max = as_int(smax)

    if n is None or min is None or max is None:
        return None

    t = str(n)
    cnt = 0
    for ch in t:
        if min <= as_int(ch) <= max:
            cnt += 1
    return cnt


def print_random_2_between(smin, smax):
    """
    Принимает числа min и max. Возвращает 3 случайных числа от min до max (включительно) без повторений.
    """
    min = as_int(smin)
    max = as_int(smax)
    res = set()

    while len(res) < 3:
        r = random.randint(min, max)
        if r not in res:
            res.add(r)
    return res


def main():
    while True:
        tn = input("Введите номер задачи Первого задания от 1 до 11 (для выхода любую другую клавишу): ")
        if tn == "1":
            help(print_hi)

            a = input("Введите число: ")
            print_hi(a)
        elif tn == "2":
            help(print_numbers)

            a = input("Введите первое число: ")
            b = input("Введите второе число: ")
            c = input("Введите третье число: ")
            print_numbers(a, b, c)
        elif tn == "3":
            help(print_count_down_100)
            print_count_down_100()
        elif tn == "4":
            help(sum_1_112_3)
            print(sum_1_112_3())
        elif tn == "5":
            help(print_mul_table)
            a = input("Введите целое число: ")
            print_mul_table(a)
        elif tn == "6":
            help(count_div)
            a = input("Введите первое число: ")
            b = input("Введите второе число: ")
            c = input("Введите третье число: ")
            print(count_div(a, b, c))
        elif tn == "7":
            help(count_user_even_numbers)
            print(count_user_even_numbers())
        elif tn == "8":
            help(print_sum_4num)

            a = input("Введите целое число: ")
            res = print_sum_4num(a)
            print("Not found" if len(res) == 0 else res)

        elif tn == "9":
            help(count_odd_num)
            a = input("Введите натуральное число: ")
            print("Количество нечетных цифр в числе: ", count_odd_num(a))
        elif tn == "10":
            help(count_num_between)
            a = input("Введите целое число: ")
            b = input("Введите min [0,9]: ")
            c = input("Введите max [0,9]: ")
            print(count_num_between(a, b, c))
        elif tn == "11":
            help(print_random_2_between)
            a = input("Введите целое min : ")
            b = input("Введите целое max: ")
            print(print_random_2_between(a, b))
        else:
            break


if __name__ == "__main__":
    main()
