def flower_with_default_vals(flower_name="ромашка", color="белый", cost=10.25):
    """
    Принимает 3 аргумента: цветок (по умолчанию “ромашка”), цвет (по умолчанию “белый”) и цена (по умолчанию 10.25).
    Делает проверку на то, что цветок и цвет - это строки, а также цена - это число больше 0, но меньше 10000.
    Выводит строку в формате “Цветок: <цветок> | Цвет: <цвет> | Цена: <цена>”.
    """
    if isinstance(flower_name, str) and isinstance(color, str) and isinstance(cost, float) and 0 < cost < 10000:
        return f"Цветок: {flower_name} | Цвет: {color} | Цена: {cost}"
    return None


def main():
    help(flower_with_default_vals)

    print(flower_with_default_vals())
    print(flower_with_default_vals(color="перламутр"))
    print(flower_with_default_vals(cost=180.37))
    print(flower_with_default_vals("незабудка", "незабудковый"))
    print(flower_with_default_vals("астра", cost=237.67))
    print(flower_with_default_vals(color="оранжевый", cost=267.87))
    print(flower_with_default_vals(cost=177.39, flower_name="календула", color="оранжевый"))


if __name__ == "__main__":
    main()
