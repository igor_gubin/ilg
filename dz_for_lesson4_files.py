import pickle


def text_file_tst(fp):
    res = ""
    with open(fp, 'r') as f:
        for line in f:
            res += line.strip() + "\n"
    return res

def bin_file_tst(fp):
    with open(fp, 'rb') as f:
        return pickle.load(f)


def save_list_to_file_classic(fp, lst):
    """
    Принимает 2 аргумента: строка (название файла или полный путь к файлу), список (для сохранения).
    Сохраняет список в файл.
    """
    with open(fp, "w") as f:
        f.write(str(lst))


def save_list_to_file_pickle(fp, lst):
    """
    Принимает 2 аргумента: строка (название файла или полный путь к файлу), список (для сохранения).
    Сохраняет список в файл.
    """
    with open(fp, "wb") as f:
        pickle.dump(lst, f)


def save_dict_to_file_classic(fp, dic):
    """
    Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).
    Сохраняет словарь в файл. Проверить, что записалось в файл.
    """
    with open(fp, "w") as f:
        f.write(str(dic))

def save_dict_to_file_pickle(fp, dic):
    """
    Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).
    Сохраняет словарь в файл.
    """
    with open(fp, "wb") as f:
        pickle.dump(dic, f)


def read_str_from_file(fp):
    """
    Принимает 1 аргумент: строка (название файла или полный путь к файлу).
    Выводит содержимое файла (в консоль).
    """
    with open(fp, 'r') as f:
        for line in f:
            print(line.strip())

def main():
    lst = ["alpha", "betta", "gamma", "delta"]
    dic = {"alpha":1, "betha":2, "gamma":3, "delta":4}
    while True:
        tn = input("Введите номер задачи Первого задания от 1 до 5 (для выхода любую другую клавишу): ")
        if tn == "1":
            fp = ".\\tst1.txt"
            help(save_list_to_file_classic)
            save_list_to_file_classic(fp, lst)
            #Проверка, что записалось в файл.
            print(text_file_tst(fp))
            continue

        if tn == "2":
            fp = ".\\tst2.txt"
            help(save_list_to_file_pickle)
            save_list_to_file_pickle(fp, lst)
            # Проверка, что записалось в файл.
            lst_tst = bin_file_tst(fp)
            print(type(lst_tst), lst == lst_tst)
            continue

        if tn == "3":
            fp = ".\\tst3.txt"

            help(save_dict_to_file_classic)
            save_dict_to_file_classic(fp, dic)
            # Проверка, что записалось в файл.
            print(text_file_tst(fp))
            continue

        if tn == "4":
            fp = ".\\tst4.txt"
            help(save_dict_to_file_pickle)
            save_dict_to_file_pickle(fp, dic)
            # Проверка, что записалось в файл.
            dic_tst = bin_file_tst(fp)
            print(type(dic_tst), dic == dic_tst)
            continue

        if tn == "5":
            fp = ".\\tst5.txt"
            help(read_str_from_file)

            contetn = "В чащах юга жил бы цитрус? Да, но фальшивый экземпляр!\n" \
                    + "Любя, съешь щипцы, — вздохнёт мэр, — кайф жгуч.\n" \
                    + "Чушь: гид вёз кэб цапф, юный жмот съел хрящ.\n" \
                    + "Съешь ещё этих мягких французских булок да выпей чаю."
            with open(fp, "w") as f:
                f.write(contetn)

            read_str_from_file(fp)

        else:
            break


if __name__ == "__main__":
    main()