def as_int(sv):
    res = 0
    try:
        res = int(sv)
    except:
        res = None
    return res

def as_float(sv):
    res = 0
    try:
        res = float(sv)
    except:
        res = None
    return res

class Dog:
    # Класс Dog . Атрибуты age, name.
    def __init__(self, name, age):
        """
        Конструктор, при создании экземпляра инициализировать атрибуты класса.
        """
        self.name = name
        self.age = age

    def print_park(self):
        """
        Печатает “Гав-гав-гав. Это мое имя name на моем собачьем языке!”
        """
        print(f"Гав-гав-гав. Это мое имя {self.name} на моем собачьем языке!")

    def get_age(self):
        """
        Возвращает значение атрибута age.
        """
        return self.age

class Zoo:
    # Класс Zoo. Атрибуты animal_count (количество животных), name и money (сколько денег на счету).
    def __init__(self, animal_count, name, money):
        """
        Конструктор. Инициализирует атрибуты класса.
        """
        self.animal_count = animal_count
        self.name = name
        self.money = money

    def get_animal_count(self):
        """
        Возвращает количество животных в зоопарке
        """
        return self.animal_count

    def print_name(self):
        """
        Печатает строку “ name - это лучший зоопарк!”
        """
        return f"{self.name} - это лучший зоопарк!"

    def can_afford(self, needed_money):
        """
        Принимает аргумент needed_money и возвращает True если зоопарк может заплатить столько денег, иначе False.
        """
        return self.money >= needed_money

class Pet:
    def __init__(self, name, corral_number, produces_per_day):
        self.name = name
        self.corral_number = corral_number
        self.produces_per_day = as_float(produces_per_day)

    def get_corral_number(self):
        return self.corral_number

    def make_sound(self):
        pass

    def get_produces_per_day(self):
        return self.produces_per_day

    def produces_type(self):
        pass

    def print_name(self):
        print(f"Name: {self.name}")

# ферма
class Goat(Pet):
    # Класс Goat.
    def __init__(self, name, corral_number, produces_per_day, age):
        super().__init__(name, corral_number, produces_per_day)
        self.age = age

    def make_sound(self):
        print("ме-е-е-е-е-е-е")
        # import winsound
        # winsound.Beep(88, 2000)

    def produces_type(self):
        return "[литров молока/день]"

    def print_name_and_age(self):
        print(f"Кличка: {self.name}; Возраст: {self.age}")

class Chicken(Pet):
    #Класс Chicken.
    def __init__(self, name, corral_number, produces_per_day):
        super().__init__(name, corral_number, produces_per_day)

    def make_sound(self):
        print("Ко ко ко")
        # import winsound
        # import time
        # winsound.Beep(37, 200)
        # time.sleep(300)
        # winsound.Beep(37, 300)
        # time.sleep(300)
        # winsound.Beep(37, 400)
        # time.sleep(300)
        # winsound.Beep(37, 500)

    def produces_type(self):
        return "[яиц/день]"

class Ferm:
    # Класс Ferm . Атрибуты: животные (list из произвольного количества Goat и Chicken), наименование
    # фермы, имя владельца фермы. Методы: напечатать наименование и владельца фермы, получить
    # количество коз на ферме (*Подсказка isinstance или type == Goat), получить количество куриц на
    # ферме, получить количество животных на ферме, получить сколько молока можно получить в день,
    # получить сколько яиц можно получить в день.
    def __init__(self, name, owner_fname, owner_mname, owner_lname):
        self.name = name

        self.owner_fname = owner_fname
        self.owner_mname = owner_mname
        self.owner_lname = owner_lname

        self.pets = []

    def pets_extend(self, *pets):
        def pets_any(pets):
            for p in pets:
                if isinstance(p, Goat) or isinstance(p, Chicken):
                    return True
            return False

        def pets_only(pets):
            for p in pets:
                if isinstance(p, Goat) or isinstance(p, Chicken):
                    yield p
            return []

        if pets_any(pets):
            self.pets.extend(pets_only(pets))

    def print_name_and_owner(self):
        owner_full_name = self.owner_fname + " " + self.owner_lname \
            if len(self.owner_mname) == 0 \
            else self.owner_fname + " " + self.owner_mname + " " + self.owner_lname
        print(f"Ферма: {self.name}; Владелец: {owner_full_name}")

    def get_pets_count(self):
        return len(self.pets)

    def get_goat_count(self):
        cnt = 0
        for p in self.pets:
            if isinstance(p, Goat):
                cnt += 1
        return cnt

    def get_chicken_count(self):
        cnt = 0
        for p in self.pets:
            if isinstance(p, Chicken):
                cnt += 1
        return cnt

    def plan_milk_liters_per_day(self):
        sum = 0.0
        for p in self.pets:
            if isinstance(p, Goat):
                sum += p.get_produces_per_day()
        return sum

    def plan_eggs_count_per_day(self):
        sum = 0
        for p in self.pets:
            if isinstance(p, Chicken):
                sum += p.get_produces_per_day()
        return sum

# итерируемые классы и наследование
class BaseIterator:
    # Базовый класс, содержит общую логику

    def __init__(self, n, step = 1):
        self.index = 0
        self.max = int(n)
        self._step = step

    def __iter__(self):
        return self

    def __next__(self):
        if self.max >= 0:
            if self.index < self.max:
                result = self.index
                self.index += self._step
                return result
            else:
                raise StopIteration()
        else:
            if self.index > self.max:
                result = self.index
                self.index -= self._step
                return result
            else:
                raise StopIteration()

class OddIntIterator(BaseIterator):
    # Класс OddIntIterator. Аргументы: целое число n . Перегрузить методы __init__ , __iter__ и __next__
    # таким образом чтобы при итерировании по экземпляру OddIntIterator метод next возвращает
    # следующее нечетное число в диапазоне [0, n ), если произошел выход за пределы этого диапазона -
    # ошибка StopIteration .

    def __init__(self, n):
        super().__init__(n, 2)

    def __next__(self):
        if self.max >= 0:
            if self.index % 2 == 0:
                self.index += 1
        else:
            if self.index % 2 == 0:
                self.index -= 1
        return super().__next__()

class FiveIterator(BaseIterator):
# 2. Класс FiveIterator . Аргументы: целое число n . Перегрузить методы __init__ , __iter__ и __next__ таким
# образом чтобы при итерировании по экземпляру StopIteration метод next возвращает следующее
# число (первое число = 0, следущее 0+5, следующее 5+5, следующее 10 + 5 и т.д) в диапазоне [0, 5,
# 10, 15, 20, …, 55, 60.. n ), если произошел выход за пределы этого диапазона - ошибка StopIteration.
    def __init__(self, n):
        super().__init__(n, 5)

# Наследование - Пример 1
class Animal:
    # Класс Animal . Аргументы: возраст, живо/мертво.
    # Методы: get_age , is_alive (True - живо, False - мертво)
    def __init__(self, age):
        self.age = int(age)
        self.alive = True

    def get_age(self):
        return self.age

    def is_alive(self):
        return self.alive

    def set_died(self):
        self.alive = False

class Domestic(Animal):
    # Класс Domestic унаследован от Animal . Аргументы: кличка, имя хозяина.
    # Методы: say_hi (печатает строку в формате “Привет! Я домашнее животное по имени <кличка>!”, get_owner_name
    def __init__(self, age, moniker, owner_name):
        Animal.__init__(self, age)
        self.moniker = str(moniker)
        self.owner_name = str(owner_name)

    def say_hi(self):
        print(f"Привет! Я домашнее животное по имени {self.moniker}!")

    def get_owner_name(self):
        return self.owner_name

class Wild(Animal):
    # Класс Wild унаследован от Animal.
    # Аргументы: страна обитания, степень опасности для человека (целое число от 0 до 5).
    # Методы: get_habitat, get_danger_rank
    def __init__(self, age, habitat, danger_rank):
        Animal.__init__(self, age)
        self.habitat = str(habitat)
        tmp = int(danger_rank)
        self.danger_rank = -1 if tmp < 0 or tmp > 5 else tmp

    def get_habitat(self):
        return self.habitat

    def get_danger_rank(self):
        return self.danger_rank

class Fox(Domestic, Wild):
    # Класс Fox унаследован от Domestic и Wild.
    # Аргументы: длина хвоста, степень пушистости хвоста (от 0 до 10).
    # Методы: say_woof (печатает строку “Гав-гав-гав!”), get_tail_lenght , get_tail_fluffy_rank
    def __init__(self, age, moniker, owner_name, habitat, danger_rank, tail_lenght, tail_fluffy_rank):
        Domestic.__init__(self, age, moniker, owner_name)
        Wild.__init__(self, age, habitat, danger_rank)
        self.tail_lenght = float(tail_lenght)
        self.tail_fluffy_rank = int(tail_fluffy_rank)

    def say_woof(self):
        print("Гав-гав-гав!")

    def get_tail_lenght(self):
        return self.tail_lenght

    def get_tail_fluffy_rank(self):
        return self.tail_fluffy_rank

# Наследование - Пример 2
class Human:
    # 2. Класс Human . Аргументы: возраст, имя. Методы: get_age , get_name
    def __init__(self, age, lname, fname, mname=""):
        self.age = age
        self.lname = str(lname).strip()
        self.fname = str(fname).strip()
        self.mname = str(mname).strip()

    def get_age(self):
        return self.age

    def get_name(self):
        return f"{self.fname} {self.lname}" if len(self.mname) == 0 else f"{self.fname} {self.mname} {self.lname}"

class Student(Human):
    # Класс Student. Аргументы: наименование ВУЗа, средний балл. Методы: get_university_name, get_average_mark
    def __init__(self, age, lname, fname, mname="", university_name="", average_mark=0.0):
        super().__init__(age, lname, fname, mname)
        self.university_name = str(university_name)
        self.average_mark = float(average_mark)

    def get_university_name(self):
        return self.university_name

    def get_average_mark(self):
        return self.average_mark;

class Worker(Human):
    # Класс Worker. Аргументы: стаж работы, текущая должность.
    # Методы: get_record_of_work, get_current_position, change_current_position
    def __init__(self, age, lname, fname, mname="", record_of_work=0.0, current_position=""):
        super().__init__(age, lname, fname, mname)
        self.record_of_work = record_of_work
        self.current_position = current_position

    def get_record_of_work(self):
        return self.record_of_work

    def get_current_position(self):
        return self.current_position

    def change_current_position(self, new_position):
        self.current_position = new_position

class StudentWorker(Student, Worker):
    # Класс StudentWorker . Аргументы: имя наставника. Методы: get_supervisor_name
    def __init__(self, age, lname, fname, mname="", \
                 record_of_work=0.0, current_position="", \
                 university_name="", average_mark=0.0,
                 supervisor_name=""):
        Student.__init__(self, age, lname, fname, mname, university_name, average_mark)
        Worker.__init__(self, age, lname, fname, mname, record_of_work, current_position)
        self.supervisor_name = str(supervisor_name)

    def get_supervisor_name(self):
        return self.supervisor_name


def main():
    while True:
        tn = input("Введите номер задачи от 1 до 7 (для выхода любую другую клавишу): ")
        if tn == "1":
            dog = Dog("Стёпка", 3)
            print("Атрибуты:", dog.__dict__)
            dog.print_park()
            print(f"{dog.name} уже большой! Ему уже {dog.get_age()} года!" )

        elif tn == "2":
            zoo = Zoo(17, "Лиловый глаз", 1222333)
            print("Атрибуты:", zoo.__dict__)
            zoo.print_name()
            print(f"Количество животных: {zoo.get_animal_count()}")
            afford = 22333
            goal = "приобретение пингвинов"
            print("Можно оплатить {} {}: {}".format(goal, afford, zoo.can_afford(afford)))
            afford = 517978
            goal = "корм льву"
            print("Можно оплатить {} {}: {}".format(goal, afford, zoo.can_afford(afford)))
            afford = 3444555
            goal = "задолженность по оплате строительства бассейна для дельфинов"
            print("Можно оплатить {} {}: {}".format(goal, afford, zoo.can_afford(afford)))

        elif tn == "3":
            f = Ferm("Заря", "Иван", "Иванович", "Иванов")
            f.pets_extend(Goat("Буся", 1, 3.54, 3.2), Goat("Туся", 1, 3.88, 4.0))
            f.pets_extend(Chicken("Мика", 2, 4), Chicken("Чука", 2, 3))
            f.print_name_and_owner()
            print(f"Общее количество домашних животных: {f.get_pets_count()}, из них")
            print(f"Количество коз: {f.get_goat_count()}")
            print(f"Количество курей: {f.get_chicken_count()}")
            print(f"План на день: молока {f.plan_milk_liters_per_day()}[л];" + \
                  f" яиц: {f.plan_eggs_count_per_day()}[шт];")
            print("===========")
            for p in f.pets:
                # print("Атрибуты: ", p.__dict__)
                print(f"Номер загона: {p.get_corral_number()}; Кличка: {p.name}")
                p.make_sound()
                print(f"Производительность: {p.get_produces_per_day()} {p.produces_type()}")
                print("-----------")

        elif tn == "4":
            n = input("Для вывода чисел от 0 до n с шагом 1, введите число n: ")
            odd_iterator = OddIntIterator(n)
            for i in odd_iterator:
                print(i)
            try:
                print(next(odd_iterator))
            except StopIteration as err:
                print("Иттерирование завершено")

        elif tn == "5":
            n = input("Для вывода чисел от 0 до n с шагом 5, введите число n: ")
            five_iterator = FiveIterator(n)
            for i in five_iterator:
                print(i)
            try:
                print(next(five_iterator))
            except StopIteration as err:
                print("Иттерирование завершено")

        elif tn == "6":
            fox = Fox(7, "Лесси", "Иван Иванов", "Россия", 3, 54, 10)
            fox.say_hi()
            fox.say_woof()
            print(f"Лисица: возраст: {fox.get_age()}; хозяин: {fox.get_owner_name()};")
            print(f"Cтрана обитания: {fox.get_habitat()}; Степень опасности: {fox.get_danger_rank()};")
            print(f"Хвост: длина: {fox.get_tail_lenght()} [см.]; пушистость: {fox.get_tail_fluffy_rank()};")

        elif tn == "7":
            s = Worker(77, "Иванов", "Иван", "Иванович", 43, "главный технолог")
            sw = StudentWorker(19, "Петров", "Петр", "Петрович", 2, \
                                           "помощник технолога", "СПбГПУ", 4.6, s.get_name())
            print(f"Наставник: {s.__dict__}")
            #print(f"Практикант: {sw.__dict__}")
            print(f"Практикант: {sw.get_name()}; возраст: {sw.get_age()} лет;")
            print(f"ВУЗ: {sw.get_university_name()}; Средняя оценка: {sw.get_average_mark()};")
            print(f"Стаж работы: {sw.get_record_of_work()} года; Должновть: {sw.get_current_position()};")
            print(f"Наставник: {sw.get_supervisor_name()}")

        else:
            break


if __name__ == "__main__":
    main()