def as_int(sv):
    res = 0
    try:
        res = int(sv)
    except:
        res = None
    return res


def as_float(sv):
    res = 0
    try:
        res = float(sv)
    except:
        res = None
    return res


def list_count_num(my_list, num):
    """
    Принимает 2 аргумента: список числами my_list (который создал пользователь) и число num.
    Возвращает количество num в списке my_list.
    """

    return my_list.count(str(num))


def list_count_word(my_list, word):
    """
    Принимает 2 аргумента: список слов my_list (который создал пользователь) и строку word.
    Возвращает количество word в списке my_list.
    """
    return my_list.count(word)


def list_count_word_num(my_list):
    """
    Принимает 1 аргумент: список “ассорти” my_list (который создал пользователь).
    Возвращает количество чисел и количество слов(букв) в списке my_list.
    """
    ncnt = 0
    wcnt = 0
    for i in my_list:
        if as_float(i) is not None:
            ncnt += 1
        else:
            wcnt += 1
    print("Количество слов: {}; Количество чисел: {};".format(wcnt, ncnt))


def list_if_found(my_list, num, word):
    """
    Принимает 3 аргумента: список “ассорти” my_list (который создал пользователь), число num и строку word.
    Возвращает “I found {word}”, если найдена только строка word;
               “I found {num}”, если найдено только число num;
               “I found {word} and {num}!!!”, если найдены оба;
               “Sorry, I can’t find anything...”, если не найдено ни слово ни число. (* тип num и word важен).
    """
    res = 0
    if num in my_list:
        res += 1
    if word in my_list:
        res += 2

    print(f"I found {word} and {num}!!!" if res == 3 \
              else f"I found {num}" if res == 1 \
        else f"I found {word}" if res == 2 \
        else f"Sorry, I can’t find anything...")


def list_magic_parts(my_list):
    """
    Принимает 1 аргумент: список “ассорти” my_list (который создал пользователь).
    Возвращает список, который состоит из [первые 2 элемента my_list]
    + [последний элемент my_list]
    + [количество элементов в списке my_list].
    """
    return my_list[0:2] + [my_list[-1], len(my_list)] if len(my_list) > 0 else []


def list_magic_mul(my_list):
    """
    Возвращает список, который состоит из
    [первого элемента my_list]
    + [три раза повторенных списков my_list]
    + [последнего элемента my_list].
    """
    return [my_list[0]] + my_list * 3 + [my_list[-1]] if len(my_list) > 0 else []


def list_magic_reversel(my_list):
    """
    Принимает 1 аргумент: список “ассорти” my_list (который создал пользователь).
    Создает новый список new_list (копия my_list), порядок которого обратный my_list.
    Возвращает список, который состоит из
    [второго элемента new_list] + [предпоследнего элемента new_list] + [весь new_list].
    """
    if len(my_list) == 0:
        return []

    new_list = list(reversed(my_list))
    lp = [new_list[2], new_list[-1]] if len(my_list) >= 3 else [new_list[-1]]
    return lp + new_list


def main():
    while True:
        tn = input("Введите номер задачи Первого задания от 1 до 7 (для выхода любую другую клавишу): ")
        if tn == "1":
            help(list_count_num)
            a = input("Введите список: ")
            b = input("Введите искомое число: ")
            la = a.split(",")
            n = as_int(b)
            print(list_count_num(la, n))
        elif tn == "2":
            help(list_count_word)
            a = input("Введите список: ")
            b = input("Введите искомое слово: ")
            la = a.split(",")
            print(list_count_num(la, b))
        elif tn == "3":
            help(list_count_word_num)
            a = input("Введите список: ")
            la = a.split(",")
            list_count_word_num(la)
        elif tn == "4":
            help(list_if_found)
            a = input("Введите список: ")
            b = input("Введите искомое число: ")
            c = input("Введите искомое слово: ")
            la = a.split(",")
            for i in range(0, len(la)):
                dtmp = as_float(la[i])
                if dtmp is not None:
                    la[i] = dtmp
            n = as_float(b)
            list_if_found(la, n, c)
        elif tn == "5":
            help(list_magic_parts)
            a = input("Введите список: ")
            la = a.split(",")
            print(list_magic_parts(la))
        elif tn == "6":
            help(list_magic_mul)
            a = input("Введите список: ")
            la = a.split(",")
            print(list_magic_mul(la))
        elif tn == "7":
            help(list_magic_reversel)
            a = input("Введите список: ")
            la = a.split(",")
            print(list_magic_reversel(la))
        else:
            break


if __name__ == "__main__":
    main()
