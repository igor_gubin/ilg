def as_int(sv):
    res = 0
    try:
        res = int(sv)
    except:
        res = None
    return res


def as_float(sv):
    res = 0
    try:
        res = float(sv)
    except:
        res = None
    return res


def arithmetic(sv1, sv2, so):
    """
    Принимает 3 аргумента: первые 2 - числа, третий - операция, которая должна быть произведена над ними. 
    Ввозвращает результат операции: если операция +, сложить их; —, вычесть; * — умножить; 
    / — разделить (первое на второе), в остальных случаях возвращает None.
    """
    v1 = as_float(sv1)
    v2 = as_float(sv2)
    o = str(so)
    res = None
    if o == "+":
        res = v1 + v2
    elif o == "-":
        res = v1 - v2
    elif o == "*":
        res = v1 * v2
    elif o == "/":
        res = v1 / v2
    return res


def is_year_leap(sv):
    """
    Принимает 1 аргумент — год. Возвращает True, если год високосный, иначе False
    """
    year = as_int(sv)
    return year is not None and year % 4 == 0 or (year % 100 == 0 and year % 400 == 0)


def month(sv):
    """
    Принимает номер месяца. Возвращает название месяца.
    Если месяца с таким  номером не существует, возвращает строку “Error”.
    """
    v = as_int(sv)
    mn = ["январь", "февраль", "март", "апрель", "май", "июнь", "июль",
          "август", "сентябрь", "октябрь", "ноябрь", "декабрь"]
    res = "Error"
    if v is not None and 1 <= v <= 12:
        res = mn[v - 1]
    return res


def print_cube_and_square(sv):
    """
    Принимает число. возвращает квадрат и куб этого числа.
    """
    v = as_float(sv)
    res = (None, None)
    if v is not None:
        sq = v * v
        res = (sq, sq * v)
    return res


def magic_arithmetic(sv1, sv2, sv3):
    """
    Принимает 3 числа. Возвращает сумму первого числа увеличенного в два раза, второго числа
    уменьшенного на 3 и третьего число возведённого в квадрат.
    """
    v1 = as_float(sv1)
    v2 = as_float(sv2)
    v3 = as_float(sv3)

    if v1 is None or v2 is None or v3 is None:
        return None
    else:
        return (v1 * 2) + (v2 - 3) + (v3 * v3)


def celsius_to_fahrenheit(sv):
    """
    Принимает значение температуры в градусах Цельсия, возвращает температуру в градусах Фаренгейта.
    """
    v = as_float(sv)
    return (v * 9 / 5 + 32) if v is not None else None


def if_3_do(sv):
    """
    Принимает число, возвращает результат преобразования: если число больше 3, то увеличивает его на 10,
    иначе уменьшает его на 10.
    """
    v = as_float(sv)
    if v is None: return None
    return v + 10 if v > 3 else v - 10


def check_numbers_by_10(sv1, sv2, sv3):
    """
    Принимает 3 числа. Если все числа больше 10 и первые два числа делятся на 3, то возврашает True, иначе False.
    """
    v1 = as_float(sv1)
    v2 = as_float(sv2)
    v3 = as_float(sv3)

    return None if v1 is None or v2 is None or v3 is None \
        else True \
        if v1 > 10 and v2 > 10 and v3 > 10 and (v1 % 3) == 0 and (v2 % 3) == 0 \
        else False


# 9. Функция check_numbers_by_magic .
def check_numbers_by_magic(sv1, sv2, sv3, sv4):
    """
    Принимает 4 числа, если первые два числа больше 5, третье число делится на 6, четвертое число не делится на 3,
    то возвращает “yes”, иначе “no”.
    """
    v1 = as_float(sv1)
    v2 = as_float(sv2)
    v3 = as_float(sv3)
    v4 = as_float(sv4)

    return None if v1 is None or v2 is None or v3 is None or v4 is None \
        else "yes" \
        if v1 > 5 and v2 > 5 and v3 % 6 == 0 and v4 % 3 == 0 \
        else "no"


def max_number (sv1, sv2, sv3):
    """
    Принимает 3 числа, возвращает наибольшее число.
    """
    v1 = as_float(sv1)
    v2 = as_float(sv2)
    v3 = as_float(sv3)

    if v1 is None or v2 is None or v3 is None:
        return None
    tmp = [v1, v2, v3]
    return max(tmp)

# 11. Функция max_sum .
def max_sum (sv1, sv2, sv3):
    """
    Принимает 3 числа. Возвращает те два из них и их сумму, если сумма наибольшая.
    """
    v1 = as_float(sv1)
    v2 = as_float(sv2)
    v3 = as_float(sv3)

    if v1 is None or v2 is None or v3 is None:
        return None

    res = -1
    max = None
    tmp = [v1 + v2, v1 + v3, v2 + v3]
    for i in range(0, 3):
        if max is None or max < tmp[i]:
            res = i
            max = tmp[i]

    return (v1, v2, max) if res == 0 \
        else (v1, v3, max) \
        if res == 1 \
        else (v2, v3, max) \
        if res == 2 else None


def sum_div_n(sv1, sv2, sv3, sn):
    """
    Принимает 3 числа и делитель. Возвращает сумму тех чисел, которые делятся на n без остатка.
    """
    v1 = as_float(sv1)
    v2 = as_float(sv2)
    v3 = as_float(sv3)
    n = as_float(sn)

    res = 0
    tmp = [v1, v2, v3]
    for i in range(0, 3):
        if tmp[i] is not None and tmp[i] % n == 0:
            res += tmp[i]
    return res


def max_even_number(sv1, sv2, sv3, sv4):
    """
    Принимает 4 числа, возвращает наибольшее четное число среди них.
    """
    res = None
    v1 = as_float(sv1)
    v2 = as_float(sv2)
    v3 = as_float(sv3)
    v4 = as_float(sv4)

    tmp = [v1, v2, v3, v4]
    for i in range(0, 4):
        if tmp[i] is not None and tmp[i] % 2 == 0 and (res is None or res < tmp[i]):
            res = tmp[i]
    return res


def check_sum(sv1, sv2, sv3):
    """
    Принимает 3 числа, возвращает True, если какие-то два из них в сумме дают третье, иначе False.
    """
    v1 = as_float(sv1)
    v2 = as_float(sv2)
    v3 = as_float(sv3)

    if v1 is None or v2 is None or v3 is None:
        return None

    res = False

    tmp1 = [v1 + v2, v1 + v3, v2 + v3]
    tmp2 = [v3, v2, v1]
    for i in range(0, 3):
        if tmp1[i] == tmp2[i]:
            res = True
            break
    return res


def count_negative(sv1, sv2, sv3, sv4):
    """
    Принимает 4 числа, возвращает количество отрицательных чисел среди них
    """
    v1 = as_float(sv1)
    v2 = as_float(sv2)
    v3 = as_float(sv3)
    v4 = as_float(sv4)

    count = 0
    for v in [v1, v2, v3, v4]:
        if v is not None and v < 0:
            count += 1
    return count


def check_num_palindrome(sv):
    """
    Принимает четырехзначное число, возвращает True, если оно читается одинаково слева направо и справа налево,
    иначе False.
    """
    res = False
    v = as_int(sv)
    if v is not None:
        t = str(v)
        if len(t) == 4:
            res = ((t[0], t[1]) == (t[3], t[2]))
    return res


def check_diff_100(sv1, sv2):
    """
    Принимает 2 числа, возвращает True, если они отличаются на 100, иначе False.
    """

    v1 = as_float(sv1)
    v2 = as_float(sv2)

    if v1 is None or v2 is None:
        return None
    mod = v1 - v2 if v1 > v2 else -(v1 - v2)
    return mod == 100


def check_minus_last_num(sv):
    """
    Принимает трехзначное число, возвращает результат вычисления выражения: число - (последняя цифра числа).
    """
    v = as_int(sv)
    t = str(v)
    res = None
    if v is not None and len(t.lstrip("+- ")) == 3:
        res = v - as_int(t[-1])
    return res

if __name__ == "__main__":
    while True:
        tn = input("Введите номер задачи Первого задания от 1 до 18 (для выхода любую другую клавишу): ")
        if tn == "1":
            help(arithmetic)

            a = input("Введите первое число: ")
            b = input("Введите второе число: ")
            c = input("Введите операцию [+,-,*,/]")

            d = arithmetic(a, b, c)
            print("Неизвестная операция" if d is None else d)

        elif tn == "2":
            help(is_year_leap)
            a = input("Введите год: ")
            print("Високосный:", is_year_leap(a))

        elif tn == "3":
            help(month)
            a = input("Введите месяц [1, 12]: ")
            print(month(a))

        elif tn == "4":
            help(print_cube_and_square)
            a = input("Введите число: ")
            r = print_cube_and_square(a)
            print("Квадрат числа:", r[0], "Куб числа:", r[1])

        elif tn == "5":
            help(magic_arithmetic)

            a = input("Введите первое число: ")
            b = input("Введите второе число: ")
            c = input("Введите третье число: ")
            print(magic_arithmetic(a, b, c))
        elif tn == "6":
            help(celsius_to_fahrenheit)

            a = input("Введите число [°C]: ")
            print(celsius_to_fahrenheit(a), "[°F]")
        elif tn == "7":
            help(if_3_do)

            a = input("Введите число: ")
            print(if_3_do(a))

        elif tn == "8":
            help(check_numbers_by_10)

            a = input("Введите первое число: ")
            b = input("Введите второе число: ")
            c = input("Введите третье число: ")
            print(check_numbers_by_10(a, b, c))
        elif tn == "9":
            help(check_numbers_by_magic)

            a = input("Введите первое число: ")
            b = input("Введите второе число: ")
            c = input("Введите третье число: ")
            d = input("Введите четвертое число: ")
            print(check_numbers_by_magic(a, b, c, d))

        elif tn == "10":
            help(max_number)

            a = input("Введите первое число: ")
            b = input("Введите второе число: ")
            c = input("Введите третье число: ")

            res = max_number(a, b, c)
            if res is None:
                print("Одно или несколько значений не числа.")
            else:
                print("Max:", res)

        elif tn == "11":
            help(max_sum)
            a = input("Введите первое число: ")
            b = input("Введите второе число: ")
            c = input("Введите третье число: ")

            res = max_sum(a, b, c)
            if res is None:
                print("Одно или несколько значений не числа.")
            else:
                print("v1:", res[0], "v2:", res[1], "sum", res[2])

        elif tn == "12":
            help(sum_div_n)

            a = input("Введите первое число: ")
            b = input("Введите второе число: ")
            c = input("Введите третье число: ")
            d = input("Введите делитель: ")

            res = sum_div_n(a, b, c, d)
            if res is None:
                print("error")
            else:
                print(res)
            print()

        elif tn == "13":
            help(max_even_number)

            a = input("Введите первое число: ")
            b = input("Введите второе число: ")
            c = input("Введите третье число: ")
            d = input("Введите четвертое число: ")

            res = max_even_number(a, b, c, d)
            if res is None:
                print("not found.")
            else:
                print(res)
            print()

        elif tn == "14":
            help(check_sum)

            a = input("Введите первое число: ")
            b = input("Введите второе число: ")
            c = input("Введите третье число: ")

            print(check_sum(a, b, c))
        elif tn == "15":
            help(count_negative)

            a = input("Введите первое число: ")
            b = input("Введите второе число: ")
            c = input("Введите третье число: ")
            d = input("Введите четвертое число: ")
            print(count_negative(a, b, c, d))

        elif tn == "16":
            help(check_num_palindrome)

            a = input("Введите первое четырехзначное целое число: ")
            print(check_num_palindrome(a))
        elif tn == "17":
            help(check_diff_100)

            a = input("Введите первое число: ")
            b = input("Введите второе число: ")
            print(check_diff_100(a,b))
        elif tn == "18":
            help(check_minus_last_num)

            a = input("Введите первое трёхзначное целое число: ")
            print(check_minus_last_num(a))
        else:
            break
