import random


def as_int(sv):
    res = 0
    try:
        res = int(sv)
    except:
        res = None
    return res


def sum_in_nums(sn1):
    sn = str(sn1)
    count = 0
    for ch in sn:
        count += as_int(ch)
    return count


def print_welcome(sn):
    """
    Принимает число n и выводит на экран фразу "You are welcome!" n раз.
    """
    n = as_int(sn)
    print("You are welcome!" * n)
    return None


def print_numbers_step1(sn):
    """
    Принимает число n и выведит на экран числа от 1 до n (включительно) с шагом 1.
    """
    n = as_int(sn)
    for i in range(n):
        if i + 1 < n:
            print(f"{i + 1}, ", end="")
        else:
            print(i + 1, end="")
    print()
    return None


def count_div_12(sv1, sv2):
    """
    Принимает целые числа a и b. Вернуть количество целых чисел от a до b включительно, которые делятся на 12.
    """
    v1 = as_int(sv1)
    v2 = as_int(sv2) + 1
    for i in range(v1, v2):
        if i % 12 == 0:
            if i + 12 < v2:
                print(f"{i}, ", end="")
            else:
                print(i, end="")
    print()
    return None


def count_user_numbers():
    """
    Пользователь вводит ненулевые числа до тех пор пока не введет ноль. Возвращает сумму этих чисел.
    """
    count = 0
    while True:
        d = input("Введите ненулевые число. Для завершения введите ноль: ")
        if d == "0":
            break
        count += as_int(d)
    return count;

def print_sum_3num(sn):
    """
    Принимает целое число n. Печатает трехзначные числа, сумма цифр которых равна n.
    Если нет ни одного такого, то печатает “Not found”.
    """
    n = as_int(sn)
    was_found = False
    for i in range(100, 1000):
        if sum_in_nums(str(i)) == n:
            if not was_found:
                print(i, end="")
            else:
                print(f", {i}", end="")
            was_found = True

    if not was_found:
        print("Not found")
    else:
        print()
    return None


def count_even_num (sn):
    """
    Принимает натуральное число, возвращает количество четных цифр в этом числе.
    """
    sn = str(sn)
    v = as_int(sn)
    if v <= 0:
        print("Введено не натуральное число")
    count = 0
    for ch in sn:
        if as_int(ch) % 2 == 0:
            count += 1
    return count


def print_random_3():
    """
    Выводит 3 случайных числа от 0 до 100 без повторений.
    """
    res = set()

    while len(res) < 3:
        r = random.randint(0, 100)
        if r not in res:
            res.add(r)

    print(res)
    return None


def check_symb(sn, cf):
    """
    Принимает строку и символ. Если символ не содержится в строке возвращает True иначе False
    """
    v = str(sn)
    res = False
    for ch in v:
        if ch == cf:
            res = True
            break
    return res


if __name__ == "__main__":
    while True:
        tn = input("Введите номер задачи Первого задания от 1 до 8 (для выхода любую другую клавишу): ")
        if tn == "1":
            print("Задача 2.1")
            help(print_welcome)

            a = input("Введите целое число: ")
            print_welcome(a)
        elif tn == "2":
            print("Задача 2.2")
            help(print_numbers_step1)

            a = input("Введите целое число: ")
            print_numbers_step1(a)
        elif tn == "3":
            print("Задача 2.1")
            help(count_div_12)

            a = input("Введите первое целое число: ")
            b = input("Введите второе целое число: ")
            count_div_12(a, b)
        elif tn == "4":
            print("Задача 2.4")
            help(count_user_numbers)

            print("Сумма введенных чисел: ", count_user_numbers())
        elif tn == "5":
            print("Задача 2.5")
            help(print_sum_3num)

            a = input("Введите целое число: ")
            print_sum_3num(a)
        elif tn == "6":
            print("Задача 2.6")
            help(count_even_num)

            a = input("Введите натуральное число: ")
            print(count_even_num(a))
        elif tn == "7":
            print("Задача 2.7")
            help(print_random_3)

            print_random_3()
        elif tn == "8":
            print("Задача 2.8")
            help(check_symb)

            a = input("Введите строку: ")
            b = input("Введите искомый символ: ")
            print("I found it!" if check_symb(a, b) else "Not found!")
        else:
            break
